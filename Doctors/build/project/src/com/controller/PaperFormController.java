package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.PaperFormDAO;
import com.gs.AddUpdatePaperForm;
import com.gs.PaperForm;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Callback;

public class PaperFormController implements Initializable {

	@FXML
	private TextField paperNumberField;
	@FXML
	private ComboBox<String> servicesBox;
	@FXML
	private TextField descrField;
	@FXML
	private TableView<PaperForm> mainTable;
	@FXML
	private TableColumn<PaperForm, Integer> numberColumn;
	@FXML
	private TableColumn<PaperForm, String> paperNumberColumn;
	@FXML
	private TableColumn<PaperForm, String> servicesColumn;
	@FXML
	private TableColumn<PaperForm, String> descrColumn;
	@FXML
	private ImageView addImage;

	private boolean checkEdit = false;
	private int idEdit;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		addImage.setImage(new Image("file:src/resources/plus.png"));

		servicesBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		servicesBox.setItems(PaperFormDAO.getServicesFromDB());

		descrColumn.setCellFactory(new Callback<TableColumn<PaperForm, String>, TableCell<PaperForm, String>>() {
			@Override
			public TableCell<PaperForm, String> call(TableColumn<PaperForm, String> param) {
				TableCell<PaperForm, String> cell = new TableCell<>();
				Text text = new Text();
				cell.setGraphic(text);
				cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
				text.wrappingWidthProperty().bind(cell.widthProperty());
				text.textProperty().bind(cell.itemProperty());
				return cell;
			}
		});

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});

		loadTable();

		ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Редактировать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				PaperForm selectedForm = mainTable.getSelectionModel().getSelectedItem();

				idEdit = selectedForm.getId();
				paperNumberField.setText(selectedForm.getNumber());
				servicesBox.setValue(selectedForm.getService());
				descrField.setText(selectedForm.getDescr());

				checkEdit = true;

				mainTable.getSelectionModel().clearSelection();
			}
		});
		MenuItem item2 = new MenuItem("Удалить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				PaperForm selectedForm = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Удаление");
				al.setHeaderText(null);
				al.setContentText("Подтвердите удаление");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					try {
						PaperFormDAO.deletePaperForm(selectedForm.getId());
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}

					loadTable();
				} else {
					al.close();
				}
			}
		});
		contextMenu.getItems().addAll(item1, item2);

		mainTable.setRowFactory(new Callback<TableView<PaperForm>, TableRow<PaperForm>>() {
			@Override
			public TableRow<PaperForm> call(TableView<PaperForm> tableView) {
				TableRow<PaperForm> row = new TableRow<>();
				row.contextMenuProperty()
						.bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));
				return row;
			}
		});
	}

	private void fitMax() {
		numberColumn.setStyle("-fx-alignment: CENTER;");
		paperNumberColumn.setStyle("-fx-alignment: CENTER;");
		servicesColumn.setStyle("-fx-alignment: CENTER;");
		descrColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		paperNumberColumn.setResizable(false);
		servicesColumn.setResizable(false);
		descrColumn.setResizable(false);
	}

	private void loadTable() {
		paperNumberColumn.setCellValueFactory(new PropertyValueFactory<PaperForm, String>("number"));
		servicesColumn.setCellValueFactory(new PropertyValueFactory<PaperForm, String>("service"));
		descrColumn.setCellValueFactory(new PropertyValueFactory<PaperForm, String>("descr"));

		mainTable.setItems(null);
		try {
			mainTable.setItems(PaperFormDAO.getTableValuesFromDB());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@FXML
	private void addAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!paperNumberField.getText().isEmpty() && servicesBox.getValue() != null
				&& !descrField.getText().isEmpty()) {
			if (paperNumberField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Номер справки' слишком большой длины!");
			} else if (descrField.getText().length() > 200) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Описание' слишком большой длины!");
			} else if (checkEdit
					&& PaperFormDAO.checkIfNumberPaperContainsInDBBeforeUpdate(paperNumberField.getText(), idEdit)) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Форма справки с таким номером уже есть в базе данных!");
			} else if (!checkEdit
					&& PaperFormDAO.checkIfNumberPaperContainsInDBBeforeInsert(paperNumberField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Форма справки с таким номером уже есть в базе данных!");
			} else {
				PaperFormDAO.addOrUpdatePaperForm(checkEdit, new AddUpdatePaperForm(idEdit, paperNumberField.getText(),
						servicesBox.getValue(), descrField.getText()));

				paperNumberField.clear();
				servicesBox.setValue(null);
				descrField.clear();

				loadTable();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void closeAction(ActionEvent event) {
		Stage parentStage = (Stage) paperNumberField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Формы справок'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}