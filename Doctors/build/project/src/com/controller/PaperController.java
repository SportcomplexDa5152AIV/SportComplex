package com.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;

import com.add.Clock;
import com.add.Validation;
import com.dao.PaperDAO;
import com.gs.Paper;
import com.main.AllScenes;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

public class PaperController implements Initializable {

	@FXML
	private Label timeLabel;
	@FXML
	private Rectangle rectangle;
	@FXML
	private ComboBox<String> paperNumberBox;
	@FXML
	private CheckBox activeCheck;
	@FXML
	private CheckBox nonActiveCheck;
	@FXML
	private TextField phoneField;
	@FXML
	private ImageView searchImage;
	@FXML
	private ImageView excelImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView paperImage;
	@FXML
	private ImageView patientsImage;
	@FXML
	private ImageView recomImage;
	@FXML
	private ImageView closeImage;
	@FXML
	private TableView<Paper> mainTable;
	@FXML
	private TableColumn<Paper, Integer> numberColumn;
	@FXML
	private TableColumn<Paper, String> paperNumberColumn;
	@FXML
	private TableColumn<Paper, String> servicesColumn;
	@FXML
	private TableColumn<Paper, String> patientsColumn;
	@FXML
	private TableColumn<Paper, String> beginDateColumn;
	@FXML
	private TableColumn<Paper, String> endDateColumn;
	@FXML
	private TableColumn<Paper, String> resultColumn;
	@FXML
	private TableColumn<Paper, String> healthColumn;
	@FXML
	private Button searchButton;

	private ObservableList<Paper> dataTable;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		paperImage.setImage(new Image("file:src/resources/paperImage.png"));
		patientsImage.setImage(new Image("file:src/resources/patientsImage.png"));
		recomImage.setImage(new Image("file:src/resources/recomImage.png"));
		searchImage.setImage(new Image("file:src/resources/search.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));
		excelImage.setImage(new Image("file:src/resources/excel.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);

		paperNumberBox.setItems(PaperDAO.getNumberPaperFromDB());

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});

		ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Редактировать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FXMLLoader fxmlLoader = new FXMLLoader();
				Parent fxmlEdit = null;
				EditPaperController editPaperController = new EditPaperController();
				try {
					fxmlLoader.setLocation(getClass().getResource("/com/models/editPaper.fxml"));
					fxmlEdit = fxmlLoader.load();
					editPaperController = fxmlLoader.getController();
				} catch (IOException e) {
				}

				Paper selectedPaper = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Редактирование");
				al.setHeaderText("Редактируем справку?");
				al.setContentText("Вы уверены, что хотите редактировать справку № " + selectedPaper.getNumber()
						+ " пациента '" + selectedPaper.getPatient() + "' ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					editPaperController.setPaper(selectedPaper);
					Stage editStage = new Stage();
					editStage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					editStage.setTitle("Редактирование");
					editStage.setWidth(505);
					editStage.setResizable(false);
					editStage.setScene(new Scene(fxmlEdit));
					editStage.initModality(Modality.WINDOW_MODAL);
					editStage.initOwner(AllScenes.primaryStage);
					editStage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
						@Override
						public void handle(javafx.stage.WindowEvent event) {
							Alert al = new Alert(Alert.AlertType.CONFIRMATION);
							al.setTitle("Закрытие");
							al.setHeaderText(null);
							al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
							Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
							Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
							okButton.setText("Да");
							Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
							cancelButton.setText("Нет");
							stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
							Optional<ButtonType> result = al.showAndWait();
							if (result.get() == ButtonType.OK) {
								editStage.close();
							} else {
								al.close();
								event.consume();
							}
						}
					});
					editStage.showAndWait();
					if (editStage.getScene().getWindow().isShowing() == false) {
						sort();
					}
					mainTable.getSelectionModel().clearSelection();
				} else {
					al.close();
				}
			}
		});
		MenuItem item2 = new MenuItem("Удалить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Paper selectedPaper = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Удаление");
				al.setHeaderText(null);
				al.setContentText("Вы уверены, что хотите удалить справку № " + selectedPaper.getNumber()
						+ " пациента '" + selectedPaper.getPatient() + "' ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					try {
						PaperDAO.deleteFromPatientPaper(selectedPaper.getId());
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}

					sort();
				} else {
					al.close();
				}
			}
		});
		contextMenu.getItems().addAll(item1, item2);

		mainTable.setRowFactory(new Callback<TableView<Paper>, TableRow<Paper>>() {
			@Override
			public TableRow<Paper> call(TableView<Paper> tableView) {
				TableRow<Paper> row = new TableRow<>();
				row.contextMenuProperty()
						.bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));
				return row;
			}
		});

		activeCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		nonActiveCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		paperNumberBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		paperNumberColumn.setStyle("-fx-alignment: CENTER;");
		servicesColumn.setStyle("-fx-alignment: CENTER;");
		patientsColumn.setStyle("-fx-alignment: CENTER;");
		beginDateColumn.setStyle("-fx-alignment: CENTER;");
		endDateColumn.setStyle("-fx-alignment: CENTER;");
		resultColumn.setStyle("-fx-alignment: CENTER;");
		healthColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		paperNumberColumn.setResizable(false);
		servicesColumn.setResizable(false);
		patientsColumn.setResizable(false);
		beginDateColumn.setResizable(false);
		endDateColumn.setResizable(false);
		resultColumn.setResizable(false);
		healthColumn.setResizable(false);
	}

	private void loadTable(String sql) {
		dataTable = FXCollections.observableArrayList();
		try {
			dataTable.setAll(PaperDAO.getTableValuesFromDB(sql));
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}

		paperNumberColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("number"));
		servicesColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("service"));
		patientsColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("patient"));
		beginDateColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("beginDate"));
		endDateColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("endDate"));
		resultColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("decision"));
		healthColumn.setCellValueFactory(new PropertyValueFactory<Paper, String>("section"));

		mainTable.setItems(null);
		try {
			mainTable.setItems(dataTable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sort() {
		Validation validation = new Validation();
		if (!phoneField.getText().isEmpty()
				&& (phoneField.getText().length() != 10 || !validation.isTelephone(phoneField.getText()))) {
			validation.alertWarning("Ошибка", null, "Поле 'Телефон' неправильного формата!");
			return;
		}

		if (paperNumberBox.getValue() != null && !paperNumberBox.getValue().equals("Все")) {
			if (((activeCheck.isSelected() && nonActiveCheck.isSelected())
					|| (!activeCheck.isSelected() && !nonActiveCheck.isSelected()))
					&& !phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`type_client_idx`) WHERE `type_id` IN (SELECT `id` FROM `doctors_paper_types` WHERE `number` = '"
								+ paperNumberBox.getValue()
								+ "') AND `client_id` IN (SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '"
								+ phoneField.getText() + "') ORDER BY `id` DESC");
			} else if (activeCheck.isSelected() && !nonActiveCheck.isSelected() && !phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`type_client_date_end_idx`) WHERE `type_id` IN (SELECT `id` FROM `doctors_paper_types` WHERE `number` = '"
								+ paperNumberBox.getValue()
								+ "') AND `client_id` IN (SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '"
								+ phoneField.getText() + "') AND `date_end` > '" + LocalDate.now()
								+ "' ORDER BY `id` DESC");
			} else if (activeCheck.isSelected() && !nonActiveCheck.isSelected() && phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`type_date_end_idx`) WHERE `type_id` IN (SELECT `id` FROM `doctors_paper_types` WHERE `number` = '"
								+ paperNumberBox.getValue() + "') AND `date_end` > '" + LocalDate.now()
								+ "' ORDER BY `id` DESC");
			} else if (!activeCheck.isSelected() && nonActiveCheck.isSelected() && !phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`type_client_date_end_idx`) WHERE `type_id` IN (SELECT `id` FROM `doctors_paper_types` WHERE `number` = '"
								+ paperNumberBox.getValue()
								+ "') AND `client_id` IN (SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '"
								+ phoneField.getText() + "') AND `date_end` <= '" + LocalDate.now()
								+ "' ORDER BY `id` DESC");
			} else if (!activeCheck.isSelected() && nonActiveCheck.isSelected() && phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`type_date_end_idx`) WHERE `type_id` IN (SELECT `id` FROM `doctors_paper_types` WHERE `number` = '"
								+ paperNumberBox.getValue() + "') AND `date_end` <= '" + LocalDate.now()
								+ "' ORDER BY `id` DESC");
			} else {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`type_id_idx`) WHERE `type_id` IN (SELECT `id` FROM `doctors_paper_types` WHERE `number` = '"
								+ paperNumberBox.getValue() + "') ORDER BY `id` DESC");
			}
		} else {
			if (((activeCheck.isSelected() && nonActiveCheck.isSelected())
					|| (!activeCheck.isSelected() && !nonActiveCheck.isSelected()))
					&& !phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`client_id_idx`) WHERE `client_id` IN (SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '"
								+ phoneField.getText() + "') ORDER BY `id` DESC");
			} else if (activeCheck.isSelected() && !nonActiveCheck.isSelected() && !phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`client_date_end_idx`) WHERE `client_id` IN (SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '"
								+ phoneField.getText() + "') AND `date_end` > '" + LocalDate.now()
								+ "' ORDER BY `id` DESC");
			} else if (activeCheck.isSelected() && !nonActiveCheck.isSelected() && phoneField.getText().isEmpty()) {
				loadTable("SELECT * FROM `doctors_paper` USE INDEX(`date_end_idx`) WHERE `date_end` > '"
						+ LocalDate.now() + "' ORDER BY `id` DESC");
			} else if (!activeCheck.isSelected() && nonActiveCheck.isSelected() && !phoneField.getText().isEmpty()) {
				loadTable(
						"SELECT * FROM `doctors_paper` USE INDEX(`client_date_end_idx`) WHERE `client_id` IN (SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '"
								+ phoneField.getText() + "') AND `date_end` <= '" + LocalDate.now()
								+ "' ORDER BY `id` DESC");
			} else if (!activeCheck.isSelected() && nonActiveCheck.isSelected() && phoneField.getText().isEmpty()) {
				loadTable("SELECT * FROM `doctors_paper` USE INDEX(`date_end_idx`) WHERE `date_end` <= '"
						+ LocalDate.now() + "' ORDER BY `id` DESC");
			} else {
				loadTable("SELECT * FROM `doctors_paper` USE INDEX() ORDER BY `id` DESC");
			}
		}
	}

	@FXML
	private void keyAction(KeyEvent event) throws Exception {
		if (event.getCode() == KeyCode.ENTER) {
			sort();
		}
	}

	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}

	@FXML
	private void addFormAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/paperForm.fxml", 905, "Формы справок",
				"Вы действительно хотите закрыть окно 'Формы справок'?");

		paperNumberBox.setItems(PaperDAO.getNumberPaperFromDB());
	}

	@FXML
	private void addPaperAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/addPaper.fxml", 505, "Создание",
				"Вы действительно хотите закрыть окно 'Создание'?");

		sort();
	}

	@SuppressWarnings("deprecation")
	@FXML
	private void excelAction(ActionEvent event) throws IOException {
		Node source = (Node) event.getSource();
		Stage parentStage = (Stage) source.getScene().getWindow();

		@SuppressWarnings("resource")
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Отчет по выданным справкам");

		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(CellUtil.BORDER_TOP, CellStyle.BORDER_MEDIUM);
		properties.put(CellUtil.BORDER_BOTTOM, CellStyle.BORDER_MEDIUM);
		properties.put(CellUtil.BORDER_LEFT, CellStyle.BORDER_MEDIUM);
		properties.put(CellUtil.BORDER_RIGHT, CellStyle.BORDER_MEDIUM);

		Map<String, Object> properties2 = new HashMap<String, Object>();
		properties2.put(CellUtil.BORDER_TOP, CellStyle.BORDER_THIN);
		properties2.put(CellUtil.BORDER_BOTTOM, CellStyle.BORDER_THIN);
		properties2.put(CellUtil.BORDER_LEFT, CellStyle.BORDER_THIN);
		properties2.put(CellUtil.BORDER_RIGHT, CellStyle.BORDER_THIN);

		HSSFCellStyle style = wb.createCellStyle();
		style.setWrapText(true);

		HSSFCellStyle style2 = wb.createCellStyle();
		HSSFFont font2 = wb.createFont();
		font2.setFontName("Arial");
		font2.setFontHeight((short) 300);
		font2.setBold(true);
		font2.setColor(HSSFColor.BLACK.index);
		style2.setFont(font2);

		HSSFCellStyle styleHeader = wb.createCellStyle();
		styleHeader.setWrapText(true);
		styleHeader.setFillForegroundColor(IndexedColors.RED.getIndex());
		styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFFont fontHeader = wb.createFont();
		fontHeader.setFontName("Arial");
		fontHeader.setFontHeight((short) 200);
		fontHeader.setBold(true);
		fontHeader.setColor(HSSFColor.BLACK.index);
		styleHeader.setFont(fontHeader);

		sheet.autoSizeColumn(2);
		sheet.autoSizeColumn(3);

		HSSFRow name = sheet.createRow(0);
		name.createCell(0).setCellValue(
				"Отчет по выданным справкам за " + LocalDate.now().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		name.setHeight((short) 800);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 7));
		name.getCell(0).setCellStyle(style2);
		CellUtil.setAlignment(name.getCell(0), HorizontalAlignment.CENTER);
		CellUtil.setVerticalAlignment((Cell) name.getCell(0), VerticalAlignment.CENTER);

		HSSFRow header = sheet.createRow(1);
		header.setHeight((short) 500);
		header.createCell(0).setCellValue("№");
		sheet.setColumnWidth(0, 2500);
		header.createCell(1).setCellValue("Номер справки");
		sheet.setColumnWidth(1, 4500);
		header.createCell(2).setCellValue("Услуга");
		sheet.setColumnWidth(2, 7500);
		header.createCell(3).setCellValue("Пациент");
		sheet.setColumnWidth(3, 7500);
		header.createCell(4).setCellValue("Дата выдачи");
		sheet.setColumnWidth(4, 3000);
		header.createCell(5).setCellValue("Дата окончания");
		sheet.setColumnWidth(5, 3000);
		header.createCell(6).setCellValue("Итог");
		sheet.setColumnWidth(6, 4500);
		header.createCell(7).setCellValue("Группа здоровья");
		sheet.setColumnWidth(7, 5500);
		for (int i = 0; i < 8; i++) {
			header.getCell(i).setCellStyle(styleHeader);
			CellUtil.setAlignment(header.getCell(i), HorizontalAlignment.CENTER);
			CellUtil.setVerticalAlignment((Cell) header.getCell(i), VerticalAlignment.CENTER);
			CellUtil.setCellStyleProperties(header.getCell(i), properties);
		}

		for (int i = 0; i < dataTable.size(); i++) {
			HSSFRow row = sheet.createRow(i + 2);
			row.createCell(0).setCellValue(i + 1);
			row.createCell(1).setCellValue(dataTable.get(i).getNumber());
			row.createCell(2).setCellValue(dataTable.get(i).getService());
			row.createCell(3).setCellValue(dataTable.get(i).getPatient());
			row.createCell(4).setCellValue(dataTable.get(i).getBeginDate());
			row.createCell(5).setCellValue(dataTable.get(i).getEndDate());
			row.createCell(6).setCellValue(dataTable.get(i).getDecision());
			row.createCell(7).setCellValue(dataTable.get(i).getSection());
			for (int j = 0; j < 8; j++) {
				row.getCell(j).setCellStyle(style);
				CellUtil.setCellStyleProperties(row.getCell(j), properties2);
				CellUtil.setVerticalAlignment((Cell) row.getCell(j), VerticalAlignment.CENTER);
				CellUtil.setAlignment(row.getCell(j), HorizontalAlignment.CENTER);
			}
		}

		FileOutputStream fileOut = new FileOutputStream("paper.xls");
		wb.write(fileOut);
		fileOut.close();

		File fileData = new File("paper.xls");
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Отчет по выданным справкам");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xls)", "*.xls");
		chooser.getExtensionFilters().add(extFilter);
		File selectedFolder = chooser.showSaveDialog(parentStage);
		if (selectedFolder != null) {
			try {
				Files.copy(fileData.toPath(), selectedFolder.toPath(), StandardCopyOption.COPY_ATTRIBUTES);

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Отчет по выданным справкам успешно создан!");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		fileData.delete();
	}

	@FXML
	private void patientsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.patientsScene);
	}

	@FXML
	private void recomAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/recom.fxml", 505, "Рекомендация",
				"Вы действительно хотите закрыть окно 'Рекомендация'?");
	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}