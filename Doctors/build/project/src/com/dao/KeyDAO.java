package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.database.DBUtil;

public class KeyDAO {

	public static boolean checkKey(String key) throws SQLException, ClassNotFoundException {
		boolean checker = false;

		Connection conn = DBUtil.getConnection();
		String sqlGetKey = "SELECT * FROM `doctor_key` WHERE `key` = '" + key + "'";
		ResultSet result = conn.createStatement().executeQuery(sqlGetKey);
		while (result.next()) {
			checker = true;
		}
		result.close();
		conn.close();

		return checker;
	}
	
}