package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.database.DBUtil;
import com.gs.Patients;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PatientsDAO {

	public static void deleteFromUserDB(int id) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();
		String sqlDelete = "UPDATE `user` SET `status` = 0 WHERE `id` = '" + id + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlDelete);
		conn.close();
	}

	public static ObservableList<Patients> getTableValuesFromDB(String sql)
			throws ClassNotFoundException, SQLException {
		ObservableList<Patients> data = FXCollections.observableArrayList();

		Connection conn = DBUtil.getConnection();
		ResultSet result = conn.createStatement().executeQuery(sql);
		while (result.next()) {
			String sex = null;
			if (result.getInt(9) == 0) {
				sex = "Мужской";
			} else {
				sex = "Женский";
			}

			data.add(new Patients(result.getInt(1), result.getString(5) + " " + result.getString(4),
					"+38" + result.getString(6),
					LocalDate.parse(result.getString(7)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), sex,
					result.getInt(10), result.getInt(11)));
		}
		result.close();
		conn.close();

		return data;
	}

	public static void addIntoDoctors(int id) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();
		String sqlDelete = "UPDATE `user` SET `doctors` = 1 WHERE `id` = '" + id + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlDelete);
		conn.close();
	}
	
	public static void deleteFromDoctors(int id) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();
		String sqlDelete = "UPDATE `user` SET `doctors` = 0 WHERE `id` = '" + id + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlDelete);
		conn.close();
	}

}