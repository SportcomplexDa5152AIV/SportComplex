package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.database.DBUtil;
import com.gs.AddUpdatePaper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AddPaperDAO {

	private static int getId(String sql) {
		int id = 0;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}

	public static ObservableList<String> getListFromDB(String sql) {
		ObservableList<String> data = FXCollections.observableArrayList();

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static String getServiceFromPaperDB(String numberPaper) {
		String service = null;

		try {
			String sqlGetService = "SELECT `service_caption` FROM `sport_services` WHERE `id` IN (SELECT `service_id` FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `number` = '"
					+ numberPaper + "' AND `status` = 1)";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetService);
			while (result.next()) {
				service = result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return service;
	}

	public static String getPatientFromUserDB(String phone) {
		String patient = null;

		try {
			String sqlGetService = "SELECT `name`,`surname` FROM `user` USE INDEX(`phone_sportclub_doctors_status_idx`) WHERE `phone` = '"
					+ phone + "' AND `sportclub` = 1 AND `doctors` = 1 AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetService);
			while (result.next()) {
				patient = result.getString(2) + " " + result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return patient;
	}

	public static void insertIntoPaperDB(AddUpdatePaper addUpdatePaper) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlInsert = "INSERT INTO `doctors_paper`(`type_id`,`client_id`,`date_begin`,`date_end`,`decision`,`section_id`) VALUES (?,?,?,?,?,?)";
		PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
		psInsert.setInt(1,
				getId("SELECT `id` FROM `doctors_paper_types` WHERE `number` = '" + addUpdatePaper.getNumber() + "'"));
		psInsert.setInt(2, getId(
				"SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '" + addUpdatePaper.getPhone() + "'"));
		psInsert.setString(3, addUpdatePaper.getBeginDate());
		psInsert.setString(4, addUpdatePaper.getEndDate());
		psInsert.setInt(5, addUpdatePaper.getDecision());
		psInsert.setInt(6, getId(
				"SELECT `id` FROM `doctors_paper_section` WHERE `caption` = '" + addUpdatePaper.getSection() + "'"));
		psInsert.execute();

		conn.close();
	}

}