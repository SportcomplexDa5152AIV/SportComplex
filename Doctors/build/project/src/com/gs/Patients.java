package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Patients {

	private final IntegerProperty id;
	private final StringProperty name;
	private final StringProperty phone;
	private final StringProperty birthday;
	private final StringProperty sex;
	private final IntegerProperty sportclub;
	private final IntegerProperty doctors;

	public Patients(Integer id, String name, String phone, String birthday, String sex, Integer sportclub,
			Integer doctors) {
		this.id = new SimpleIntegerProperty(id);
		this.doctors = new SimpleIntegerProperty(doctors);
		this.sportclub = new SimpleIntegerProperty(sportclub);
		this.name = new SimpleStringProperty(name);
		this.phone = new SimpleStringProperty(phone);
		this.birthday = new SimpleStringProperty(birthday);
		this.sex = new SimpleStringProperty(sex);
	}

	public Integer getId() {
		return id.get();
	}

	public IntegerProperty idProperty() {
		return id;
	}

	public void setId(Integer id) {
		this.id.set(id);
	}

	public Integer getDoctors() {
		return doctors.get();
	}

	public IntegerProperty doctorsProperty() {
		return doctors;
	}

	public void setDoctors(Integer doctors) {
		this.doctors.set(doctors);
	}

	public Integer getSportclub() {
		return sportclub.get();
	}

	public IntegerProperty sportclubProperty() {
		return sportclub;
	}

	public void setSportclub(Integer sportclub) {
		this.sportclub.set(sportclub);
	}

	public String getName() {
		return name.get();
	}

	public StringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getPhone() {
		return phone.get();
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}

	public String getSex() {
		return sex.get();
	}

	public StringProperty sexProperty() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex.set(sex);
	}

	public String getBirthday() {
		return birthday.get();
	}

	public StringProperty birthdayProperty() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday.set(birthday);
	}

}