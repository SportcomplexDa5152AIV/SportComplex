package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddUpdateUser {

	private final StringProperty name;
	private final StringProperty surname;
	private final StringProperty phone;
	private final StringProperty birthday;
	private final IntegerProperty sex;
	private final IntegerProperty sportclub;
	private final IntegerProperty doctors;

	public AddUpdateUser(String name, String surname, String phone, String birtday, Integer sex, Integer sportclub,
			Integer doctors) {
		this.sex = new SimpleIntegerProperty(sex);
		this.name = new SimpleStringProperty(name);
		this.phone = new SimpleStringProperty(phone);
		this.surname = new SimpleStringProperty(surname);
		this.birthday = new SimpleStringProperty(birtday);
		this.sportclub = new SimpleIntegerProperty(sportclub);
		this.doctors = new SimpleIntegerProperty(doctors);
	}

	public String getName() {
		return name.get();
	}

	public StringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public Integer getSex() {
		return sex.get();
	}

	public IntegerProperty sexProperty() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex.set(sex);
	}
	
	public Integer getSportclub() {
		return sportclub.get();
	}

	public IntegerProperty sportclubProperty() {
		return sportclub;
	}

	public void setSportclub(Integer sportclub) {
		this.sportclub.set(sportclub);
	}
	
	public Integer getDoctors() {
		return doctors.get();
	}

	public IntegerProperty doctorsProperty() {
		return doctors;
	}

	public void setDoctors(Integer doctors) {
		this.doctors.set(doctors);
	}

	public String getSurname() {
		return surname.get();
	}

	public StringProperty surnameProperty() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname.set(surname);
	}

	public String getPhone() {
		return phone.get();
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}

	public String getBirthday() {
		return birthday.get();
	}

	public StringProperty birthdayProperty() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday.set(birthday);
	}

}