package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddUpdatePaperForm {

	private final IntegerProperty idEdit;
	private final StringProperty number;
	private final StringProperty service;
	private final StringProperty descr;

	public AddUpdatePaperForm(Integer idEdit, String number, String service, String descr) {
		this.idEdit = new SimpleIntegerProperty(idEdit);
		this.number = new SimpleStringProperty(number);
		this.service = new SimpleStringProperty(service);
		this.descr = new SimpleStringProperty(descr);
	}

	public String getNumber() {
		return number.get();
	}

	public StringProperty numberProperty() {
		return number;
	}

	public void setNumber(String number) {
		this.number.set(number);
	}

	public Integer getIdEdit() {
		return idEdit.get();
	}

	public IntegerProperty idEditProperty() {
		return idEdit;
	}

	public void setIdEdit(Integer idEdit) {
		this.idEdit.set(idEdit);
	}

	public String getService() {
		return service.get();
	}

	public StringProperty serviceProperty() {
		return service;
	}

	public void setService(String service) {
		this.service.set(service);
	}

	public String getDescr() {
		return descr.get();
	}

	public StringProperty descrProperty() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr.set(descr);
	}
	
}