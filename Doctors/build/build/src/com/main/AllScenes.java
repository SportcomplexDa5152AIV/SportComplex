package com.main;

import java.io.IOException;
import java.net.BindException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.database.DBUtil;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class AllScenes extends Application {

	public static Scene keyScene;
	public static Scene loginScene;
	public static Scene paperScene;
	public static Scene patientsScene;
	public static Stage primaryStage;

	private static final int PORT = 9999;
	@SuppressWarnings("unused")
	private static ServerSocket socket;

	@Override
	public void start(Stage primaryStage) throws IOException, SQLException, ClassNotFoundException {
		Connection conn = DBUtil.getConnection();

		if (conn == null) {
			Validation validation = new Validation();
			validation.alertWarning("Ошибка", "Нет подключения", "Проверьте, пожалуйста, соединение с интернетом...");
			System.exit(0);
		}

		boolean checkRegister = false;
		String sqlGetFromLogin = "SELECT * FROM `doctor_login`";
		ResultSet result = conn.createStatement().executeQuery(sqlGetFromLogin);
		while (result.next()) {
			checkRegister = true;
		}
		result.close();

		conn.close();

		primaryStage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Выход");
				al.setHeaderText(null);
				al.setContentText("Вы уверены, что хотите выйти?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					System.exit(0);
				} else {
					al.close();
					event.consume();
				}
			}
		});

		if (checkRegister) {
			Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();

			loginScene = new Scene(FXMLLoader.load(getClass().getResource("/com/models/login.fxml")));

			primaryStage.setX(visualBounds.getMinX());
			primaryStage.setY(visualBounds.getMinY());
			primaryStage.setWidth(visualBounds.getWidth());
			primaryStage.setHeight(visualBounds.getHeight());
			primaryStage.setScene(loginScene);
			primaryStage.setTitle("Doctors");
		} else {
			keyScene = new Scene(FXMLLoader.load(getClass().getResource("/com/models/key.fxml")), 290, 170);

			primaryStage.setScene(keyScene);
			primaryStage.setTitle("Регистрация");
		}
		primaryStage.setResizable(false);
		primaryStage.centerOnScreen();
		primaryStage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		primaryStage.show();

		AllScenes.primaryStage = primaryStage;
	}

	private static void checkIfRunning() {
		try {
			socket = new ServerSocket(PORT, 0, InetAddress.getByAddress(new byte[] { 127, 0, 0, 1 }));
		} catch (BindException e) {
			System.exit(1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(2);
		}
	}

	public static void main(String[] args) {
		checkIfRunning();
		launch(args);
	}

}