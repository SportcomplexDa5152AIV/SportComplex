package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.AddUpdatePaper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class EditPaperDAO {

	private static int getId(String sql) {
		int id = 0;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}

	public static ObservableList<String> getListFromDB(String sql) {
		ObservableList<String> data = FXCollections.observableArrayList();

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static String getServiceFromPaperDB(String numberPaper) {
		String service = null;

		try {
			String sqlGetService = "SELECT `service_caption` FROM `sport_services` WHERE `id` IN (SELECT `service_id` FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `number` = '"
					+ numberPaper + "' AND `status` = 1)";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetService);
			while (result.next()) {
				service = result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return service;
	}

	public static void updateIntoPaperDB(int id, AddUpdatePaper addUpdatePaper)
			throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlUpdate = "UPDATE `doctors_paper` SET `type_id` = '"
				+ getId("SELECT `id` FROM `doctors_paper_types` WHERE `number` = '" + addUpdatePaper.getNumber() + "'")
				+ "', `date_end` = '" + addUpdatePaper.getEndDate() + "', `decision` = '" + addUpdatePaper.getDecision()
				+ "', `section_id` = '" + getId("SELECT `id` FROM `doctors_paper_section` WHERE `caption` = '"
						+ addUpdatePaper.getSection() + "'")
				+ "' WHERE `id` = '" + id + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlUpdate);
		conn.close();
	}

}