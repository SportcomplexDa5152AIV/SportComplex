package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.add.Validation;
import com.database.DBUtil;

public class ChangePasswordDAO {

	public static boolean checkKey(String key) throws SQLException, ClassNotFoundException {
		boolean checker = false;

		Connection conn = DBUtil.getConnection();
		String sqlGetKey = "SELECT * FROM `doctor_key` WHERE `key` = '" + key + "'";
		ResultSet result = conn.createStatement().executeQuery(sqlGetKey);
		while (result.next()) {
			checker = true;
		}
		result.close();
		conn.close();

		return checker;
	}

	public static void updatePassword(String password) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		String sqlUpdate = "UPDATE `doctor_login` SET `password` = '" + validation.md5Apache(password) + "'";
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlUpdate);
		conn.close();
	}

}