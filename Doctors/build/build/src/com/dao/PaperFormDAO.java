package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.AddUpdatePaperForm;
import com.gs.PaperForm;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PaperFormDAO {

	private static int getId(String sql) {
		int id = 0;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}

	private static String getString(String sql) {
		String text = null;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				text = result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return text;
	}

	public static ObservableList<String> getServicesFromDB() {
		ObservableList<String> data = FXCollections.observableArrayList();

		try {
			String sqlGetSection = "SELECT `service_caption` FROM `sport_services` USE INDEX(`need_status_idx`) WHERE `need_clinic` = 1 AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetSection);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static boolean checkIfNumberPaperContainsInDBBeforeInsert(String numberPaper) {
		boolean checker = false;

		try {
			String sqlGetPaperNumber = "SELECT * FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `number` = '"
					+ numberPaper + "' AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetPaperNumber);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static boolean checkIfNumberPaperContainsInDBBeforeUpdate(String numberPaper, int idEdit) {
		boolean checker = false;

		try {
			String sqlGetPaperNumber = "SELECT * FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `number` = '"
					+ numberPaper + "' AND `id` <> '" + idEdit + "' AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetPaperNumber);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static ObservableList<PaperForm> getTableValuesFromDB() throws ClassNotFoundException, SQLException {
		ObservableList<PaperForm> data = FXCollections.observableArrayList();

		String sqlGetPaperForm = "SELECT * FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `status` = 1";
		Connection conn = DBUtil.getConnection();
		ResultSet result = conn.createStatement().executeQuery(sqlGetPaperForm);
		while (result.next()) {
			data.add(new PaperForm(result.getInt(1), result.getString(2),
					getString("SELECT `service_caption` FROM `sport_services` WHERE `id` = '" + result.getInt(3) + "'"),
					result.getString(4)));
		}
		result.close();
		conn.close();

		return data;
	}

	public static void addOrUpdatePaperForm(boolean checkEdit, AddUpdatePaperForm addUpdatePaperForm)
			throws SQLException, ClassNotFoundException {
		Connection conn = DBUtil.getConnection();

		if (checkEdit) {
			String sqlUpdate = "UPDATE `doctors_paper_types` SET `number` = '" + addUpdatePaperForm.getNumber()
					+ "', `service_id` = '"
					+ getId("SELECT `id` FROM `sport_services` WHERE `service_caption` = '"
							+ addUpdatePaperForm.getService() + "'")
					+ "', `description` = '" + addUpdatePaperForm.getDescr() + "' WHERE `id` = '"
					+ addUpdatePaperForm.getIdEdit() + "'";
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sqlUpdate);
		} else {
			String sqlInsert = "INSERT INTO `doctors_paper_types`(`number`,`service_id`,`description`) VALUES (?,?,?)";
			PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
			psInsert.setString(1, addUpdatePaperForm.getNumber());
			psInsert.setInt(2, getId("SELECT `id` FROM `sport_services` WHERE `service_caption` = '"
					+ addUpdatePaperForm.getService() + "'"));
			psInsert.setString(3, addUpdatePaperForm.getDescr());
			psInsert.execute();
		}

		conn.close();
	}

	public static void deletePaperForm(int idDelete) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();
		String sqlDelete = "UPDATE `doctors_paper_types` SET `status` = 0 WHERE `id` = '" + idDelete + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlDelete);
		conn.close();
	}

}