package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.add.Validation;
import com.database.DBUtil;
import com.gs.Doctors;

public class RegistrationDAO {

	public static void insertDoctorsLogin(Doctors doctors) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		Connection conn = DBUtil.getConnection();

		String sqlInsertLogin = "INSERT INTO `doctor_login`(`login`,`password`) VALUES (?,?)";
		PreparedStatement psInsertLogin = conn.prepareStatement(sqlInsertLogin);
		psInsertLogin.setString(1, doctors.getLogin());
		psInsertLogin.setString(2, validation.md5Apache(doctors.getPassword()));
		psInsertLogin.execute();

		conn.close();
	}

}