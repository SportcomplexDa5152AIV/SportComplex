package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.database.DBUtil;
import com.gs.AddUpdateUser;

public class AddPatientDAO {

	public static boolean checkIfPhoneContainsInDB(String phone) {
		boolean checker = false;

		try {
			String sqlGetPaperNumber = "SELECT * FROM `user` USE INDEX(`phone_status_idx`) WHERE `phone` = '" + phone
					+ "' AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetPaperNumber);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static void insertIntoUserDB(AddUpdateUser addUpdateUser) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlInsert = "INSERT INTO `user`(`name`,`surname`,`phone`,`birthday`,`sex`,`sportclub`,`doctors`) VALUES (?,?,?,?,?,?,?)";
		PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
		psInsert.setString(1, addUpdateUser.getName());
		psInsert.setString(2, addUpdateUser.getSurname());
		psInsert.setString(3, addUpdateUser.getPhone());
		psInsert.setString(4, addUpdateUser.getBirthday());
		psInsert.setInt(5, addUpdateUser.getSex());
		psInsert.setInt(6, addUpdateUser.getSportclub());
		psInsert.setInt(7, addUpdateUser.getDoctors());
		psInsert.execute();

		conn.close();
	}

}