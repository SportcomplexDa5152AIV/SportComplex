package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;

import com.database.DBUtil;
import com.gs.Recomended;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class RecommendedDAO {

	private static int getId(String sql) {
		int id = 0;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}
	
	public static ObservableList<String> getServicesFromDB() {
		ObservableList<String> data = FXCollections.observableArrayList();

		try {
			String sqlGetSection = "SELECT `service_caption` FROM `sport_services` USE INDEX(`status_idx`) WHERE `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetSection);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static String getPatientFromUserDB(String phone) {
		String patient = null;

		try {
			String sqlGetService = "SELECT `name`,`surname` FROM `user` USE INDEX(`phone_doctors_status_idx`) WHERE `phone` = '"
					+ phone + "' AND `doctors` = 1 AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetService);
			while (result.next()) {
				patient = result.getString(2) + " " + result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return patient;
	}

	public static void insertIntoAddAbonementDB(Recomended recomended) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlInsert = "INSERT INTO `doctors_add_abonement`(`datetime`,`client_id`,`service_id`,`cause`,`agree`) VALUES (?,?,?,?,?)";
		PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
		psInsert.setString(1, LocalDateTime.now().toString());
		psInsert.setInt(2, getId(
				"SELECT `id` FROM `user` USE INDEX(`phone_idx`) WHERE `phone` = '" + recomended.getPhone() + "'"));
		psInsert.setInt(3,
				getId("SELECT `id` FROM `sport_services` WHERE `service_caption` = '" + recomended.getService() + "'"));
		psInsert.setString(4, recomended.getCause());
		psInsert.setInt(5, recomended.getDecision());
		psInsert.execute();

		conn.close();
	}

}