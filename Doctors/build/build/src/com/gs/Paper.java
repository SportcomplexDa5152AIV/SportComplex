package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Paper {

	private final IntegerProperty id;
	private final StringProperty number;
	private final StringProperty service;
	private final StringProperty patient;
	private final StringProperty beginDate;
	private final StringProperty endDate;
	private final StringProperty decision;
	private final StringProperty section;
	private final StringProperty phone;

	public Paper(Integer id, String number, String service, String patient, String beginDate, String endDate,
			String decision, String section, String phone) {
		this.id = new SimpleIntegerProperty(id);
		this.decision = new SimpleStringProperty(decision);
		this.service = new SimpleStringProperty(service);
		this.number = new SimpleStringProperty(number);
		this.patient = new SimpleStringProperty(patient);
		this.beginDate = new SimpleStringProperty(beginDate);
		this.endDate = new SimpleStringProperty(endDate);
		this.section = new SimpleStringProperty(section);
		this.phone = new SimpleStringProperty(phone);
	}

	public Integer getId() {
		return id.get();
	}

	public IntegerProperty idProperty() {
		return id;
	}

	public void setId(Integer id) {
		this.id.set(id);
	}

	public String getNumber() {
		return number.get();
	}

	public StringProperty numberProperty() {
		return number;
	}

	public void setNumber(String number) {
		this.number.set(number);
	}
	
	public String getPhone() {
		return phone.get();
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}

	public String getDecision() {
		return decision.get();
	}

	public StringProperty decisionProperty() {
		return decision;
	}

	public void setDecision(String decision) {
		this.decision.set(decision);
	}

	public String getSection() {
		return section.get();
	}

	public StringProperty sectionProperty() {
		return section;
	}

	public void setSection(String section) {
		this.section.set(section);
	}

	public String getPatient() {
		return patient.get();
	}

	public StringProperty patientProperty() {
		return patient;
	}

	public void setPatient(String phone) {
		this.patient.set(phone);
	}

	public String getBeginDate() {
		return beginDate.get();
	}

	public StringProperty beginDateProperty() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate.set(beginDate);
	}

	public String getEndDate() {
		return endDate.get();
	}

	public StringProperty endDateProperty() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate.set(endDate);
	}
	
	public String getService() {
		return service.get();
	}

	public StringProperty serviceProperty() {
		return service;
	}

	public void setService(String service) {
		this.service.set(service);
	}

}