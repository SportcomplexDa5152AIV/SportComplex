package com.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Clock;
import com.add.Validation;
import com.dao.PatientsDAO;
import com.gs.Patients;
import com.main.AllScenes;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

public class PatientsController implements Initializable {

	@FXML
	private Label timeLabel;
	@FXML
	private Rectangle rectangle;
	@FXML
	private ComboBox<String> sexBox;
	@FXML
	private TextField phoneField;
	@FXML
	private ImageView searchImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView paperImage;
	@FXML
	private ImageView patientsImage;
	@FXML
	private ImageView recomImage;
	@FXML
	private ImageView closeImage;
	@FXML
	private TableView<Patients> mainTable;
	@FXML
	private TableColumn<Patients, Integer> numberColumn;
	@FXML
	private TableColumn<Patients, String> nameColumn;
	@FXML
	private TableColumn<Patients, String> phoneColumn;
	@FXML
	private TableColumn<Patients, String> birthdayColumn;
	@FXML
	private TableColumn<Patients, String> sexColumn;
	@FXML
	private Button searchButton;
	@FXML
	private CheckBox clientsCheck;
	@FXML
	private Button addIntoDoctorsButton;

	private Validation validation = new Validation();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		paperImage.setImage(new Image("file:src/resources/paperImage.png"));
		patientsImage.setImage(new Image("file:src/resources/patientsImage.png"));
		recomImage.setImage(new Image("file:src/resources/recomImage.png"));
		searchImage.setImage(new Image("file:src/resources/search.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);

		fillSexBox();

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});

		ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Редактировать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (clientsCheck.isSelected()) {
					validation.alertWarning("Ошибка", null,
							"Редактирование или Удаление данных о клиентах спорткомплекса невозможно!");
				} else {
					FXMLLoader fxmlLoader = new FXMLLoader();
					Parent fxmlEdit = null;
					EditPatientController editPatientController = new EditPatientController();
					try {
						fxmlLoader.setLocation(getClass().getResource("/com/models/editPatient.fxml"));
						fxmlEdit = fxmlLoader.load();
						editPatientController = fxmlLoader.getController();
					} catch (IOException e) {
					}

					Patients selectedPatient = mainTable.getSelectionModel().getSelectedItem();
					Alert al = new Alert(Alert.AlertType.CONFIRMATION);
					al.setTitle("Редактирование");
					al.setHeaderText("Редактируем справку?");
					al.setContentText("Вы уверены, что хотите редактировать данные о пациенте '"
							+ selectedPatient.getName() + "' ?");
					Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
					Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
					okButton.setText("Да");
					Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
					cancelButton.setText("Нет");
					stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					Optional<ButtonType> result = al.showAndWait();
					if (result.get() == ButtonType.OK) {
						al.close();
						editPatientController.setPatient(selectedPatient);
						Stage editStage = new Stage();
						editStage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
						editStage.setTitle("Редактирование");
						editStage.setWidth(505);
						editStage.setResizable(false);
						editStage.setScene(new Scene(fxmlEdit));
						editStage.initModality(Modality.WINDOW_MODAL);
						editStage.initOwner(AllScenes.primaryStage);
						editStage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
							@Override
							public void handle(javafx.stage.WindowEvent event) {
								Alert al = new Alert(Alert.AlertType.CONFIRMATION);
								al.setTitle("Закрытие");
								al.setHeaderText(null);
								al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
								Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
								Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
								okButton.setText("Да");
								Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
								cancelButton.setText("Нет");
								stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
								Optional<ButtonType> result = al.showAndWait();
								if (result.get() == ButtonType.OK) {
									editStage.close();
								} else {
									al.close();
									event.consume();
								}
							}
						});
						editStage.showAndWait();
						if (editStage.getScene().getWindow().isShowing() == false) {
							sort();
						}
						mainTable.getSelectionModel().clearSelection();
					} else {
						al.close();
					}
				}
			}
		});
		MenuItem item2 = new MenuItem("Удалить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (clientsCheck.isSelected()) {
					validation.alertWarning("Ошибка", null,
							"Редактирование или Удаление данных о клиентах спорткомплекса невозможно!");
				} else {
					Patients selectedPatient = mainTable.getSelectionModel().getSelectedItem();
					Alert al = new Alert(Alert.AlertType.CONFIRMATION);
					al.setTitle("Удаление");
					al.setHeaderText(null);
					al.setContentText(
							"Вы уверены, что хотите удалить данные о пациенте '" + selectedPatient.getName() + "' ?");
					Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
					Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
					okButton.setText("Да");
					Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
					cancelButton.setText("Нет");
					stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					Optional<ButtonType> result = al.showAndWait();
					if (result.get() == ButtonType.OK) {
						al.close();
						try {
							if (selectedPatient.getSportclub() == 0) {
								PatientsDAO.deleteFromUserDB(selectedPatient.getId());
							} else {
								PatientsDAO.deleteFromDoctors(selectedPatient.getId());
							}
						} catch (ClassNotFoundException | SQLException e) {
							e.printStackTrace();
						}

						sort();
					} else {
						al.close();
					}
				}
			}
		});
		contextMenu.getItems().addAll(item1, item2);

		mainTable.setRowFactory(new Callback<TableView<Patients>, TableRow<Patients>>() {
			@Override
			public TableRow<Patients> call(TableView<Patients> tableView) {
				TableRow<Patients> row = new TableRow<>();
				row.contextMenuProperty()
						.bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));
				return row;
			}
		});

		sexBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		clientsCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (clientsCheck.isSelected()) {
					addIntoDoctorsButton.setDisable(false);
				} else {
					addIntoDoctorsButton.setDisable(true);
				}

				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		nameColumn.setStyle("-fx-alignment: CENTER;");
		phoneColumn.setStyle("-fx-alignment: CENTER;");
		birthdayColumn.setStyle("-fx-alignment: CENTER;");
		sexColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		nameColumn.setResizable(false);
		phoneColumn.setResizable(false);
		birthdayColumn.setResizable(false);
		sexColumn.setResizable(false);

		numberColumn.setPrefWidth(visualBounds.getWidth() * 0.04);
		nameColumn.setPrefWidth(visualBounds.getWidth() * 0.36);
		phoneColumn.setPrefWidth(visualBounds.getWidth() * 0.17);
		birthdayColumn.setPrefWidth(visualBounds.getWidth() * 0.11);
		sexColumn.setPrefWidth(visualBounds.getWidth() * 0.12);
	}

	private void fillSexBox() {
		ObservableList<String> data = FXCollections.observableArrayList();
		data.addAll("Все", "Мужской", "Женский");
		sexBox.setItems(data);
	}

	private void loadTable(String sql) {
		ObservableList<Patients> dataTable = FXCollections.observableArrayList();
		try {
			dataTable.setAll(PatientsDAO.getTableValuesFromDB(sql));
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}

		nameColumn.setCellValueFactory(new PropertyValueFactory<Patients, String>("name"));
		phoneColumn.setCellValueFactory(new PropertyValueFactory<Patients, String>("phone"));
		birthdayColumn.setCellValueFactory(new PropertyValueFactory<Patients, String>("birthday"));
		sexColumn.setCellValueFactory(new PropertyValueFactory<Patients, String>("sex"));

		mainTable.setItems(null);
		try {
			mainTable.setItems(dataTable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sort() {
		if (!phoneField.getText().isEmpty()
				&& (phoneField.getText().length() != 10 || !validation.isTelephone(phoneField.getText()))) {
			validation.alertWarning("Ошибка", null, "Поле 'Телефон' неправильного формата!");
			return;
		}

		int sex = 0;
		if (sexBox.getValue() != null && !sexBox.getValue().equals("Все")) {
			if (sexBox.getValue().equals("Женский")) {
				sex = 1;
			}
		}

		if (!clientsCheck.isSelected()) {
			if (!phoneField.getText().isEmpty() && sexBox.getValue() != null && !sexBox.getValue().equals("Все")) {
				loadTable("SELECT * FROM `user` USE INDEX(`phone_sex_doctors_status_idx`) WHERE `phone` = '"
						+ phoneField.getText() + "' AND `sex` = '" + sex + "' AND `doctors` = 1 AND `status` = 1");
			} else if (phoneField.getText().isEmpty() && sexBox.getValue() != null
					&& !sexBox.getValue().equals("Все")) {
				loadTable("SELECT * FROM `user` USE INDEX(`sex_doctors_status_idx`) WHERE `sex` = '" + sex
						+ "' AND `doctors` = 1 AND `status` = 1");
			} else if (!phoneField.getText().isEmpty()
					&& (sexBox.getValue() == null || (sexBox.getValue() != null && sexBox.getValue().equals("Все")))) {
				loadTable("SELECT * FROM `user` USE INDEX(`phone_doctors_status_idx`) WHERE `phone` = '"
						+ phoneField.getText() + "' AND `doctors` = 1 AND `status` = 1");
			} else {
				loadTable("SELECT * FROM `user` USE INDEX(`doctors_status_idx`) WHERE `doctors` = 1 AND `status` = 1");
			}
		} else {
			if (!phoneField.getText().isEmpty() && sexBox.getValue() != null && !sexBox.getValue().equals("Все")) {
				loadTable("SELECT * FROM `user` USE INDEX(`phone_sex_sportclub_status_idx`) WHERE `phone` = '"
						+ phoneField.getText() + "' AND `sex` = '" + sex + "' AND `sportclub` = 1 AND `status` = 1");
			} else if (phoneField.getText().isEmpty() && sexBox.getValue() != null
					&& !sexBox.getValue().equals("Все")) {
				loadTable("SELECT * FROM `user` USE INDEX(`sex_sportclub_status_idx`) WHERE `sex` = '" + sex
						+ "' AND `sportclub` = 1 AND `status` = 1");
			} else if (!phoneField.getText().isEmpty()
					&& (sexBox.getValue() == null || (sexBox.getValue() != null && sexBox.getValue().equals("Все")))) {
				loadTable("SELECT * FROM `user` USE INDEX(`phone_sportclub_status_idx`) WHERE `phone` = '"
						+ phoneField.getText() + "' AND `sportclub` = 1 AND `status` = 1");
			} else {
				loadTable(
						"SELECT * FROM `user` USE INDEX(`sportclub_status_idx`) WHERE `sportclub` = 1 AND `status` = 1");
			}
		}
	}

	@FXML
	private void keyAction(KeyEvent event) throws Exception {
		if (event.getCode() == KeyCode.ENTER) {
			sort();
		}
	}

	@FXML
	private void addIntoDoctorsAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Patients selectedPatient = mainTable.getSelectionModel().getSelectedItem();
		if (selectedPatient == null) {
			validation.alertWarning("Ошибка", null, "Выберите клиента, пожалуйста!");
		} else if (selectedPatient.getDoctors() == 1) {
			validation.alertWarning("Ошибка", null, "Данный клиент уже стоит на учете у врача!");
		} else {
			Alert al = new Alert(Alert.AlertType.CONFIRMATION);
			al.setTitle("Выход");
			al.setHeaderText(null);
			al.setContentText("Вы уверены, что хотите поставить на учет данного клиента?");
			Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
			Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
			okButton.setText("Да");
			Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
			cancelButton.setText("Нет");
			stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
			Optional<ButtonType> result = al.showAndWait();
			if (result.get() == ButtonType.OK) {
				PatientsDAO.addIntoDoctors(selectedPatient.getId());
			} else {
				al.close();
			}
		}
		mainTable.getSelectionModel().clearSelection();

		sort();
	}

	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}

	@FXML
	private void addPatientAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/addPatient.fxml", 505, "Создание",
				"Вы действительно хотите закрыть окно 'Создание'?");
		
		sort();
	}

	@FXML
	private void papersAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.paperScene);
	}

	@FXML
	private void recomAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/recom.fxml", 505, "Рекомендация",
				"Вы действительно хотите закрыть окно 'Рекомендация'?");
	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}