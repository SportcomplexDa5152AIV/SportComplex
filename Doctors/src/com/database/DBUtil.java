package com.database;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class DBUtil {

	private static InputStream is;
	private static String CONNECTION;
	private static String USER;
	private static String PASSWORD;

	@Rule
	public final ExpectedException thrown = ExpectedException.none();

	@Test
	public void getFile() throws IOException {
		thrown.expect(IOException.class);
		Properties properties = new Properties();
		is = new FileInputStream("control.properties");
		properties.load(is);
	}
	
	public static Connection getConnection() throws SQLException, ClassNotFoundException {
		Properties properties = new Properties();
		
		try {
			is = new FileInputStream("control.properties");
			properties.load(is);

			CONNECTION = "jdbc:mysql://" + properties.getProperty("database") + "/sport?autoReconnect=true&useSSL=false";
			USER = properties.getProperty("dbuser");
			PASSWORD = properties.getProperty("dbpassword");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(CONNECTION, USER, PASSWORD);
	}
	
}