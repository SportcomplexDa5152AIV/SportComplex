package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.AddUpdateUser;

public class EditPatientDAO {

	public static boolean checkIfPhoneContainsInDB(int id, String phone) {
		boolean checker = false;

		try {
			String sqlGetPaperNumber = "SELECT * FROM `user` USE INDEX(`phone_status_idx`) WHERE `phone` = '" + phone
					+ "' AND `status` = 1 AND `id` <> '" + id + "'";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetPaperNumber);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static void updateUserDB(int id, AddUpdateUser addUpdateUser) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlUpdate = "UPDATE `user` SET `name` = '" + addUpdateUser.getName() + "', `surname` = '"
				+ addUpdateUser.getSurname() + "', `phone` = '" + addUpdateUser.getPhone() + "', `birthday` = '"
				+ addUpdateUser.getBirthday() + "', `sex` = '" + addUpdateUser.getSex() + "' WHERE `id` = '" + id + "'";
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlUpdate);

		conn.close();
	}

}