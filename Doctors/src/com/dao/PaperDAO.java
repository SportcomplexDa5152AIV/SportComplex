package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.database.DBUtil;
import com.gs.Paper;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class PaperDAO {

	private static String getString(String sql) {
		String text = null;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				text = result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return text;
	}

	public static ObservableList<String> getNumberPaperFromDB() {
		ObservableList<String> data = FXCollections.observableArrayList();

		data.add("Все");
		try {
			String sqlGetSection = "SELECT `number` FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetSection);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static void deleteFromPatientPaper(int id) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();
		String sqlDelete = "DELETE FROM `doctors_paper` WHERE `id` = '" + id + "'";
		PreparedStatement ps = conn.prepareStatement(sqlDelete);
		ps.execute();
		conn.close();
	}

	public static ObservableList<Paper> getTableValuesFromDB(String sql) throws ClassNotFoundException, SQLException {
		ObservableList<Paper> data = FXCollections.observableArrayList();

		Connection conn = DBUtil.getConnection();
		ResultSet result = conn.createStatement().executeQuery(sql);
		while (result.next()) {
			String decision = null;
			if (result.getInt(6) == 0) {
				decision = "Не допущен";
			} else {
				decision = "Допущен";
			}

			data.add(new Paper(result.getInt(1),
					getString("SELECT `number` FROM `doctors_paper_types` WHERE `id` = '" + result.getInt(2) + "'"),
					getString(
							"SELECT `service_caption` FROM `sport_services` WHERE `id` IN (SELECT `service_id` FROM `doctors_paper_types` WHERE `id` = '"
									+ result.getInt(2) + "')"),
					getString("SELECT CONCAT(`surname`, \" \", `name`) FROM `user` WHERE `id` = '" + result.getInt(3)
							+ "'"),
					LocalDate.parse(result.getString(4)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
					LocalDate.parse(result.getString(5)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")), decision,
					getString("SELECT `caption` FROM `doctors_paper_section` WHERE `id` = '" + result.getInt(7) + "'"),
					getString("SELECT `phone` FROM `user` WHERE `id` = '" + result.getInt(3) + "'")));
		}
		result.close();
		conn.close();

		return data;
	}

}