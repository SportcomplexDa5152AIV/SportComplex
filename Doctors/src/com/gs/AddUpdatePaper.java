package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddUpdatePaper {

	private final StringProperty number;
	private final StringProperty phone;
	private final StringProperty beginDate;
	private final StringProperty endDate;
	private final IntegerProperty decision;
	private final StringProperty section;

	public AddUpdatePaper(String number, String phone, String beginDate, String endDate, Integer decision, String section) {
		this.decision = new SimpleIntegerProperty(decision);
		this.number = new SimpleStringProperty(number);
		this.phone = new SimpleStringProperty(phone);
		this.beginDate = new SimpleStringProperty(beginDate);
		this.endDate = new SimpleStringProperty(endDate);
		this.section = new SimpleStringProperty(section);
	}

	public String getNumber() {
		return number.get();
	}

	public StringProperty numberProperty() {
		return number;
	}

	public void setNumber(String number) {
		this.number.set(number);
	}

	public Integer getDecision() {
		return decision.get();
	}

	public IntegerProperty decisionProperty() {
		return decision;
	}

	public void setDecision(Integer decision) {
		this.decision.set(decision);
	}

	public String getSection() {
		return section.get();
	}

	public StringProperty sectionProperty() {
		return section;
	}

	public void setSection(String section) {
		this.section.set(section);
	}

	public String getPhone() {
		return phone.get();
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}
	
	public String getBeginDate() {
		return beginDate.get();
	}

	public StringProperty beginDateProperty() {
		return beginDate;
	}

	public void setBeginDate(String beginDate) {
		this.beginDate.set(beginDate);
	}
	
	public String getEndDate() {
		return endDate.get();
	}

	public StringProperty endDateProperty() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate.set(endDate);
	}
	
}