package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Recomended {

	private final StringProperty phone;
	private final StringProperty service;
	private final StringProperty cause;
	private final IntegerProperty decision;

	public Recomended(String phone, String service, String cause, Integer decision) {
		this.decision = new SimpleIntegerProperty(decision);
		this.service = new SimpleStringProperty(service);
		this.phone = new SimpleStringProperty(phone);
		this.cause = new SimpleStringProperty(cause);
	}

	public String getService() {
		return service.get();
	}

	public StringProperty serviceProperty() {
		return service;
	}

	public void setService(String service) {
		this.service.set(service);
	}

	public Integer getDecision() {
		return decision.get();
	}

	public IntegerProperty decisionProperty() {
		return decision;
	}

	public void setDecision(Integer decision) {
		this.decision.set(decision);
	}

	public String getCause() {
		return cause.get();
	}

	public StringProperty causeProperty() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause.set(cause);
	}

	public String getPhone() {
		return phone.get();
	}

	public StringProperty phoneProperty() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone.set(phone);
	}

}