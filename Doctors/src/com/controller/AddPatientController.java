package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.AddPatientDAO;
import com.gs.AddUpdateUser;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class AddPatientController implements Initializable {

	@FXML
	private TextField nameField;
	@FXML
	private TextField surnameField;
	@FXML
	private TextField phoneField;
	@FXML
	private ComboBox<String> dateBox;
	@FXML
	private ComboBox<String> monthBox;
	@FXML
	private ComboBox<String> yearBox;
	@FXML
	private CheckBox maleCheck;
	@FXML
	private CheckBox femaleCheck;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		dateBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		monthBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		yearBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		fillDayBox();
		fillMonthBox();
		fillYearBox();

		maleCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (femaleCheck.isSelected()) {
					femaleCheck.setSelected(false);
				}
			}
		});

		femaleCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (maleCheck.isSelected()) {
					maleCheck.setSelected(false);
				}
			}
		});
	}

	private void fillDayBox() {
		ObservableList<String> days = FXCollections.observableArrayList();
		days.addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16",
				"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31");
		this.dateBox.setItems(days);
	}

	private void fillMonthBox() {
		ObservableList<String> month = FXCollections.observableArrayList();
		month.addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
		this.monthBox.setItems(month);
	}

	private void fillYearBox() {
		ObservableList<String> year = FXCollections.observableArrayList();
		for (int i = LocalDate.now().getYear(); i >= LocalDate.now().getYear() - 100; i--) {
			year.addAll(String.valueOf(i));
		}
		this.yearBox.setItems(year);
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!nameField.getText().isEmpty() && !surnameField.getText().isEmpty() && !phoneField.getText().isEmpty()
				&& (maleCheck.isSelected() || femaleCheck.isSelected()) && dateBox.getValue() != null
				&& monthBox.getValue() != null && yearBox.getValue() != null) {
			if (nameField.getText().length() > 100) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Имя' слишком большой длины!");
			} else if (!validation.isAlphabeticWithoutSpace(nameField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Имя' неправильного формата!");
			} else if (surnameField.getText().length() > 100) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Фамилия' слишком большой длины!");
			} else if (!validation.isAlphabeticWithoutSpace(surnameField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Фамилия' неправильного формата!");
			} else if (phoneField.getText().length() != 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Телефон' должно быть длиной в 10 символов!");
			} else if (!validation.isTelephone(phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Телефон' неправильного формата!");
			} else if (AddPatientDAO.checkIfPhoneContainsInDB(phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Пациент с таким номером телефона уже есть в базе данных!");
			} else {
				int sex = 0;
				if (femaleCheck.isSelected()) {
					sex = 1;
				}

				String birthday = null;
				try {
					birthday = LocalDate.parse(
							this.yearBox.getValue() + "-" + this.monthBox.getValue() + "-" + this.dateBox.getValue())
							.toString();
				} catch (Exception e) {
					validation.alertWarning("Ошибка", "Неверный формат полей", "Неверный формат поля 'Дата Рождения'!");
					return;
				}

				AddPatientDAO.insertIntoUserDB(new AddUpdateUser(nameField.getText(), surnameField.getText(),
						phoneField.getText(), birthday, sex, 0, 1));

				Stage stage = (Stage) nameField.getScene().getWindow();
				stage.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) nameField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Создание'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}