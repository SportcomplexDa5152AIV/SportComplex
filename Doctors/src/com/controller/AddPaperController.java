package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.AddPaperDAO;
import com.gs.AddUpdatePaper;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class AddPaperController implements Initializable {

	@FXML
	private ComboBox<String> numberPaperBox;
	@FXML
	private TextField servicesField;
	@FXML
	private TextField phoneField;
	@FXML
	private TextField patientField;
	@FXML
	private DatePicker beginDateField;
	@FXML
	private DatePicker endDateField;
	@FXML
	private CheckBox yesCheck;
	@FXML
	private CheckBox noCheck;
	@FXML
	private ComboBox<String> healthBox;

	private Validation validation = new Validation();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		numberPaperBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		healthBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		numberPaperBox.setItems(AddPaperDAO.getListFromDB(
				"SELECT `number` FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `status` = 1"));
		healthBox.setItems(AddPaperDAO.getListFromDB("SELECT `caption` FROM `doctors_paper_section`"));

		numberPaperBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				searchServiceInPaperForm();
			}
		});

		phoneField.textProperty().addListener((o, oldText, newText) -> {
			searchPatientInUser();
		});

		yesCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (noCheck.isSelected()) {
					noCheck.setSelected(false);
				}
			}
		});

		noCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (yesCheck.isSelected()) {
					yesCheck.setSelected(false);
				}
			}
		});
	}

	private void searchServiceInPaperForm() {
		servicesField.setText(AddPaperDAO.getServiceFromPaperDB(numberPaperBox.getValue()));
	}

	private void searchPatientInUser() {
		patientField.clear();
		if (!phoneField.getText().isEmpty() && phoneField.getText().length() == 10
				&& validation.isTelephone(phoneField.getText())) {
			patientField.setText(AddPaperDAO.getPatientFromUserDB(phoneField.getText()));
		}
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		if (numberPaperBox.getValue() != null && !phoneField.getText().isEmpty() && beginDateField.getValue() != null
				&& endDateField.getValue() != null && (yesCheck.isSelected() || noCheck.isSelected())
				&& healthBox.getValue() != null) {
			if (phoneField.getText().length() != 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Телефон' должно быть длиной в 10 символов!");
			} else if (!validation.isTelephone(phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Телефон' неправильного формата!");
			} else if (beginDateField.getValue().isAfter(endDateField.getValue())
					|| beginDateField.getValue().isBefore(LocalDate.now())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Поля 'Выдано'/'Действ. до' неправильного формата!");
			} else if (patientField.getText() == null) {
				validation.alertWarning("Ошибка", null,
						"Похоже, что пациент с таким номером телефона не стоит на учете в поликлинике и/или не является клиентом спорткомплекса!");
			} else {
				int decision = 0;
				if (yesCheck.isSelected()) {
					decision = 1;
				}

				AddPaperDAO.insertIntoPaperDB(new AddUpdatePaper(numberPaperBox.getValue(), phoneField.getText(),
						beginDateField.getValue().toString(), endDateField.getValue().toString(), decision,
						healthBox.getValue()));

				Stage stage = (Stage) numberPaperBox.getScene().getWindow();
				stage.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) numberPaperBox.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Создание'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}