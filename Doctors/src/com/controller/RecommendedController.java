package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.RecommendedDAO;
import com.gs.Recomended;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class RecommendedController implements Initializable {

	@FXML
	private TextField phoneField;
	@FXML
	private TextField patientField;
	@FXML
	private ComboBox<String> servicesBox;
	@FXML
	private TextArea causeField;
	@FXML
	private CheckBox agreeCheck;

	private Validation validation = new Validation();

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		servicesBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		servicesBox.setItems(RecommendedDAO.getServicesFromDB());

		phoneField.textProperty().addListener((o, oldText, newText) -> {
			searchPatientInUser();
		});
	}

	private void searchPatientInUser() {
		patientField.clear();
		if (!phoneField.getText().isEmpty() && phoneField.getText().length() == 10
				&& validation.isTelephone(phoneField.getText())) {
			patientField.setText(RecommendedDAO.getPatientFromUserDB(phoneField.getText()));
		}
	}

	@FXML
	private void sendAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!phoneField.getText().isEmpty() && servicesBox.getValue() != null && !causeField.getText().isEmpty()) {
			if (phoneField.getText().length() != 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Телефон' должно быть длиной в 10 символов!");
			} else if (!validation.isTelephone(phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Телефон' неправильного формата!");
			} else if (causeField.getText().length() > 300) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Причина рекомендации' слишком большой длины!");
			} else {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Выход");
				al.setHeaderText(null);
				al.setContentText("Вы действительно хотите отправить рекомендацию администратору спорткомплекса?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					int decision = 0;
					if (agreeCheck.isSelected()) {
						decision = 1;
					}

					RecommendedDAO.insertIntoAddAbonementDB(new Recomended(phoneField.getText(), servicesBox.getValue(),
							causeField.getText(), decision));

					Alert alert = new Alert(Alert.AlertType.INFORMATION);
					alert.setTitle("Уведомление");
					alert.setHeaderText(null);
					alert.setContentText("Рекомендация успешно отправлена!");
					Stage stage3 = (Stage) alert.getDialogPane().getScene().getWindow();
					stage3.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					alert.showAndWait();
					Stage stage2 = (Stage) phoneField.getScene().getWindow();
					stage2.close();
				} else {
					al.close();
				}
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) phoneField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Рекомендация'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}