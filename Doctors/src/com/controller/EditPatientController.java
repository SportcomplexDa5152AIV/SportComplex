package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.EditPatientDAO;
import com.gs.AddUpdateUser;
import com.gs.Patients;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class EditPatientController implements Initializable {

	@FXML
	private TextField nameField;
	@FXML
	private TextField surnameField;
	@FXML
	private TextField phoneField;
	@FXML
	private ComboBox<String> dateBox;
	@FXML
	private ComboBox<String> monthBox;
	@FXML
	private ComboBox<String> yearBox;
	@FXML
	private CheckBox maleCheck;
	@FXML
	private CheckBox femaleCheck;
	@FXML
	private Button saveButton;

	private Patients patients;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		maleCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (femaleCheck.isSelected()) {
					femaleCheck.setSelected(false);
				}
			}
		});

		femaleCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (maleCheck.isSelected()) {
					maleCheck.setSelected(false);
				}
			}
		});
	}

	public void setPatient(Patients patients) {
		this.patients = patients;

		if (patients.getSportclub() == 1) {
			nameField.setEditable(false);
			surnameField.setEditable(false);
			phoneField.setEditable(false);
			maleCheck.setDisable(true);
			femaleCheck.setDisable(true);
			saveButton.setDisable(true);
		} else {
			fillDayBox();
			fillMonthBox();
			fillYearBox();
		}

		nameField.setText(patients.getName().substring(patients.getName().indexOf(" ") + 1));
		surnameField.setText(patients.getName().substring(0, patients.getName().indexOf(" ")));
		phoneField.setText(patients.getPhone().substring(3));
		dateBox.setValue(patients.getBirthday().substring(0, 2));
		monthBox.setValue(patients.getBirthday().substring(3, 5));
		yearBox.setValue(patients.getBirthday().substring(6, 10));
		if (patients.getSex().equals("Мужской")) {
			maleCheck.setSelected(true);
		} else {
			femaleCheck.setSelected(true);
		}
	}

	private void fillDayBox() {
		ObservableList<String> days = FXCollections.observableArrayList();
		days.addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16",
				"17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31");
		this.dateBox.setItems(days);
	}

	private void fillMonthBox() {
		ObservableList<String> month = FXCollections.observableArrayList();
		month.addAll("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
		this.monthBox.setItems(month);
	}

	private void fillYearBox() {
		ObservableList<String> year = FXCollections.observableArrayList();
		for (int i = LocalDate.now().getYear(); i >= LocalDate.now().getYear() - 100; i--) {
			year.addAll(String.valueOf(i));
		}
		this.yearBox.setItems(year);
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!nameField.getText().isEmpty() && !surnameField.getText().isEmpty() && !phoneField.getText().isEmpty()
				&& (maleCheck.isSelected() || femaleCheck.isSelected()) && dateBox.getValue() != null
				&& monthBox.getValue() != null && yearBox.getValue() != null) {
			if (nameField.getText().length() > 100) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Имя' слишком большой длины!");
			} else if (!validation.isAlphabeticWithoutSpace(nameField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Имя' неправильного формата!");
			} else if (surnameField.getText().length() > 100) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Фамилия' слишком большой длины!");
			} else if (!validation.isAlphabeticWithoutSpace(surnameField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Фамилия' неправильного формата!");
			} else if (phoneField.getText().length() != 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Телефон' должно быть длиной в 10 символов!");
			} else if (!validation.isTelephone(phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Телефон' неправильного формата!");
			} else if (EditPatientDAO.checkIfPhoneContainsInDB(patients.getId(), phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Пациент с таким номером телефона уже есть в базе данных!");
			} else {
				int sex = 0;
				if (femaleCheck.isSelected()) {
					sex = 1;
				}

				String birthday = null;
				try {
					birthday = LocalDate.parse(
							this.yearBox.getValue() + "-" + this.monthBox.getValue() + "-" + this.dateBox.getValue())
							.toString();
				} catch (Exception e) {
					validation.alertWarning("Ошибка", "Неверный формат полей", "Неверный формат поля 'Дата Рождения'!");
					return;
				}

				EditPatientDAO.updateUserDB(patients.getId(), new AddUpdateUser(nameField.getText(),
						surnameField.getText(), phoneField.getText(), birthday, sex, 0, 0));

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Данные успешно изменены!");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
				Stage stage2 = (Stage) nameField.getScene().getWindow();
				stage2.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) nameField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}