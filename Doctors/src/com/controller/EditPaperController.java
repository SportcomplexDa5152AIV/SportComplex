package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.EditPaperDAO;
import com.gs.AddUpdatePaper;
import com.gs.Paper;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class EditPaperController implements Initializable {

	@FXML
	private ComboBox<String> numberPaperBox;
	@FXML
	private TextField servicesField;
	@FXML
	private TextField phoneField;
	@FXML
	private TextField patientField;
	@FXML
	private DatePicker beginDateField;
	@FXML
	private DatePicker endDateField;
	@FXML
	private CheckBox yesCheck;
	@FXML
	private CheckBox noCheck;
	@FXML
	private ComboBox<String> healthBox;

	private Paper paper;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		numberPaperBox.setItems(EditPaperDAO.getListFromDB(
				"SELECT `number` FROM `doctors_paper_types` USE INDEX(`status_idx`) WHERE `status` = 1"));
		healthBox.setItems(EditPaperDAO.getListFromDB("SELECT `caption` FROM `doctors_paper_section`"));

		numberPaperBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				searchServiceInPaperForm();
			}
		});

		yesCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (noCheck.isSelected()) {
					noCheck.setSelected(false);
				}
			}
		});

		noCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (yesCheck.isSelected()) {
					yesCheck.setSelected(false);
				}
			}
		});
	}

	public void setPaper(Paper paper) {
		this.paper = paper;
		numberPaperBox.setValue(paper.getNumber());
		servicesField.setText(paper.getService());
		phoneField.setText(paper.getPhone());
		patientField.setText(paper.getPatient());
		beginDateField.setValue(LocalDate.parse(paper.getBeginDate(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		endDateField.setValue(LocalDate.parse(paper.getEndDate(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		if (paper.getDecision().equals("Допущен")) {
			yesCheck.setSelected(true);
		} else {
			noCheck.setSelected(true);
		}
		healthBox.setValue(paper.getSection());
	}

	private void searchServiceInPaperForm() {
		servicesField.setText(EditPaperDAO.getServiceFromPaperDB(numberPaperBox.getValue()));
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (numberPaperBox.getValue() != null && !phoneField.getText().isEmpty() && beginDateField.getValue() != null
				&& endDateField.getValue() != null && (yesCheck.isSelected() || noCheck.isSelected())
				&& healthBox.getValue() != null) {
			if (phoneField.getText().length() != 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Телефон' должно быть длиной в 10 символов!");
			} else if (!validation.isTelephone(phoneField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!", "Поле 'Телефон' неправильного формата!");
			} else if (beginDateField.getValue().isAfter(endDateField.getValue())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Поля 'Выдано'/'Действ. до' неправильного формата!");
			} else {
				int decision = 0;
				if (yesCheck.isSelected()) {
					decision = 1;
				}

				EditPaperDAO.updateIntoPaperDB(paper.getId(),
						new AddUpdatePaper(numberPaperBox.getValue(), phoneField.getText(),
								beginDateField.getValue().toString(), endDateField.getValue().toString(), decision,
								healthBox.getValue()));

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Данные успешно изменены!");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
				Stage stage2 = (Stage) numberPaperBox.getScene().getWindow();
				stage2.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) numberPaperBox.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}