[Setup]
AppId={{1D50C7F9-ECA3-4D61-B700-7E8A9A045EE7}
AppName=SportClub
AppVersion=1.0
;AppVerName=Parus 1.0
AppPublisher=Detox Sky
AppPublisherURL=https://ivi-soft.com/
AppSupportURL=https://ivi-soft.com/
AppUpdatesURL=https://ivi-soft.com/
DefaultDirName={sd}\SportClub
DisableProgramGroupPage=yes
OutputDir=D:\
OutputBaseFilename=SportClub-installer
SetupIconFile=D:\Programs\Work\SportClub-utils\resources\SportClub_resources\SportClub.ico
Compression=lzma
SolidCompression=yes
ArchitecturesInstallIn64BitMode=x64

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"
Name: "russian"; MessagesFile: "compiler:Languages\Russian.isl"
Name: "ukrainian"; MessagesFile: "compiler:Languages\Ukrainian.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Users\__x_Concorde_x__\AppData\Local\SportClub\SportClub.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "C:\Users\__x_Concorde_x__\AppData\Local\SportClub\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{commonprograms}\SportClub"; Filename: "{app}\SportClub.exe"
Name: "{commondesktop}\SportClub"; Filename: "{app}\SportClub.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\SportClub.exe"; Description: "{cm:LaunchProgram,Parus}"; Flags: nowait postinstall skipifsilent

