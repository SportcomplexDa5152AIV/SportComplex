package com.add;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.digest.DigestUtils;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class Validation {

	public String md5Apache(String st) {
	    String md5Hex = DigestUtils.md5Hex(st);
	    return md5Hex;
	}

	public void alertWarning(String title, String header, String content) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		alert.showAndWait();
	}
	
	public boolean isTelephone(String s) {
		return s != null && s.matches("[0-9]*");
	}
	
	public boolean isNumeric(String s) {
		return s != null && s.matches("[+]?\\d*\\.?\\d*");
	}
	
	public boolean isAlphabeticWithoutSpace(String s) {
		return s != null && s.matches("[a-zA-Zа-яА-Я]+");
	}
	
	public boolean isValidEmailAddress(String email) {
		String ePattern = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern p = Pattern.compile(ePattern);
		Matcher m = p.matcher(email);
		return m.matches();
	}
	
	public double round(double number, int scale) {
		int pow = 10;
		for (int i = 1; i < scale; i++)
			pow *= 10;
		double tmp = number * pow;
		return (double) (int) ((tmp - (int) tmp) >= 0.5 ? tmp + 1 : tmp) / pow;
	}
	
}