package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.database.DBUtil;
import com.gs.AddUpdateExpend;

public class AddExpendDAO {

	public static void insertExpendIntoDB(AddUpdateExpend addUpdateExpend) throws SQLException, ClassNotFoundException {
		Connection conn = DBUtil.getConnection();

		String sqlInsert = "INSERT INTO `sport_balance_outputs`(`caption`,`date`,`price`) VALUES (?,?,?)";
		PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
		psInsert.setString(1, addUpdateExpend.getName());
		psInsert.setString(2, addUpdateExpend.getDate());
		psInsert.setDouble(3, addUpdateExpend.getPrice());
		psInsert.execute();

		conn.close();
	}
}