package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.add.Validation;
import com.database.DBUtil;
import com.gs.Admin;

public class RegistrationDAO {

	public static void insertAdminLogin(Admin admin) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		Connection conn = DBUtil.getConnection();

		String sqlInsertLogin = "INSERT INTO `admin_login`(`login`,`password`,`mail`,`password_mail`,`login_sms`,`password_sms`,`alpha_sms`) VALUES (?,?,?,?,?,?,?)";
		PreparedStatement psInsertLogin = conn.prepareStatement(sqlInsertLogin);
		psInsertLogin.setString(1, admin.getLogin());
		psInsertLogin.setString(2, validation.md5Apache(admin.getPassword()));
		psInsertLogin.setString(3, admin.getLoginMail());
		psInsertLogin.setString(4, admin.getPasswordMail());
		psInsertLogin.setString(5, admin.getLoginSms());
		psInsertLogin.setString(6, admin.getPasswordSms());
		psInsertLogin.setString(7, admin.getAlpha());
		psInsertLogin.execute();

		conn.close();
	}

}