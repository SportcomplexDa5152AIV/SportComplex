package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.Settings;

public class SettingsDAO {

	public static Settings getSettingsValues() {
		Settings settings = null;

		try {
			String sqlGetLogin = "SELECT `login`,`mail`,`login_sms`,`alpha_sms` FROM `admin_login`";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetLogin);
			while (result.next()) {
				settings = new Settings(result.getString(1), result.getString(2), result.getString(3),
						result.getString(4));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return settings;
	}

	public static void updateAction(String sql) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sql);
		conn.close();
	}

}