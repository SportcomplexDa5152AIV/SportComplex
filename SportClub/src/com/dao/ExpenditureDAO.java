package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import com.database.DBUtil;
import com.gs.Expend;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ExpenditureDAO {

	public static ObservableList<Expend> getTableValuesFromDB(String sql) throws ClassNotFoundException, SQLException {
		ObservableList<Expend> data = FXCollections.observableArrayList();

		Connection conn = DBUtil.getConnection();
		ResultSet result = conn.createStatement().executeQuery(sql);
		while (result.next()) {
			data.add(new Expend(result.getInt(1), result.getString(2),
					LocalDate.parse(result.getString(3)).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")),
					result.getDouble(4)));
		}
		result.close();
		conn.close();

		return data;
	}

	public static void deleteFromExpendDB(int id) throws ClassNotFoundException, SQLException {
		String sqlDelete = "DELETE FROM `sport_balance_outputs` WHERE `id` = '" + id + "'";
		Connection conn = DBUtil.getConnection();
		PreparedStatement ps = conn.prepareStatement(sqlDelete);
		ps.execute();
		conn.close();
	}

}