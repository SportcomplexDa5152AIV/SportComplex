package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.AddUpdateService;

public class EditServiceDAO {

	public static boolean checkIfServiceContainsInDBBeforeUpdate(int id, String section) {
		boolean checker = false;

		try {
			String sqlGet = "SELECT * FROM `sport_services` WHERE `service_caption` = '" + section
					+ "' AND `status` = 1 AND `id` <> '" + id + "'";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGet);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static void updateService(int id, AddUpdateService addUpdateService)
			throws ClassNotFoundException, SQLException {
		String sqlUpdate = "UPDATE `sport_services` SET `service_caption` = '" + addUpdateService.getName()
				+ "', `service_price_single` = '" + addUpdateService.getOnce() + "', `service_price_month` = '"
				+ addUpdateService.getMonth() + "', `service_price_year` = '" + addUpdateService.getYear()
				+ "', `need_clinic` = '" + addUpdateService.getDoctors() + "' WHERE `id` = '" + id + "'";
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlUpdate);
		conn.close();
	}

}