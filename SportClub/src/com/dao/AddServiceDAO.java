package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.database.DBUtil;
import com.gs.AddUpdateService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class AddServiceDAO {

	private static int getId(String sql) {
		int id = 0;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				id = result.getInt(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return id;
	}
	
	public static boolean checkIfServiceContainsInDBBeforeInsert(String section) {
		boolean checker = false;

		try {
			String sqlGet = "SELECT * FROM `sport_services` WHERE `service_caption` = '" + section
					+ "' AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGet);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static ObservableList<String> getListOfSectionsFromDB() {
		ObservableList<String> data = FXCollections.observableArrayList();

		try {
			String sqlGet = "SELECT `caption` FROM `sport_service_sections` WHERE `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGet);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static void insertIntoPaperDB(AddUpdateService addUpdateService)
			throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlInsert = "INSERT INTO `sport_services`(`service_caption`,`service_section_id`,`service_price_single`,`service_price_month`,`service_price_year`,`need_clinic`) VALUES (?,?,?,?,?,?)";
		PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
		psInsert.setString(1, addUpdateService.getName());
		psInsert.setInt(2, getId(
				"SELECT `id` FROM `sport_service_sections` WHERE `caption` = '" + addUpdateService.getSection() + "'"));
		psInsert.setDouble(3, addUpdateService.getOnce());
		psInsert.setDouble(4, addUpdateService.getMonth());
		psInsert.setDouble(5, addUpdateService.getYear());
		psInsert.setInt(6, addUpdateService.getDoctors());
		psInsert.execute();

		conn.close();
	}

}