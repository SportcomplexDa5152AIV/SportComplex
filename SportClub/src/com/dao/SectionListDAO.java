package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.SectionList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class SectionListDAO {

	public static ObservableList<SectionList> fillSectionListFromDB() {
		ObservableList<SectionList> data = FXCollections.observableArrayList();

		try {
			String sql = "SELECT * FROM `sport_service_sections` WHERE `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				data.add(new SectionList(result.getInt(1), result.getString(2)));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static void deleteSection(int id) throws SQLException, ClassNotFoundException {
		String sqlDelete = "UPDATE `sport_service_sections` SET `status` = 0 WHERE `id` = '" + id + "'";
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlDelete);
		conn.close();
	}

}