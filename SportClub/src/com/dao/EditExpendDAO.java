package com.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.AddUpdateExpend;

public class EditExpendDAO {

	public static void updateExpendIntoDB(int id, AddUpdateExpend addUpdateExpend) throws ClassNotFoundException, SQLException {
		String sqlUpdate = "UPDATE `sport_balance_outputs` SET `caption` = '" + addUpdateExpend.getName()
				+ "', `date` = '" + addUpdateExpend.getDate() + "', `price` = '" + addUpdateExpend.getPrice()
				+ "' WHERE `id` = '" + id + "'";
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlUpdate);
		conn.close();
	}

}