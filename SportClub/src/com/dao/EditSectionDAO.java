package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;

public class EditSectionDAO {

	public static boolean checkIfSectionContainsInDBBeforeUpdate(int id, String section) {
		boolean checker = false;

		try {
			String sqlGet = "SELECT * FROM `sport_service_sections` WHERE `caption` = '" + section
					+ "' AND `status` = 1 AND `id` <> '" + id + "'";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGet);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static void updateIntoDB(int id, String section) throws ClassNotFoundException, SQLException {
		String sqlUpdate = "UPDATE `sport_service_sections` SET `caption` = '" + section + "' WHERE `id` = '" + id
				+ "'";
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlUpdate);
		conn.close();
	}

}