package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.database.DBUtil;

public class AddSectionDAO {

	public static boolean checkIfSectionContainsInDBBeforeInsert(String section) {
		boolean checker = false;

		try {
			String sqlGet = "SELECT * FROM `sport_service_sections` WHERE `caption` = '" + section
					+ "' AND `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGet);
			if (result.next()) {
				checker = true;
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return checker;
	}

	public static void insertIntoDB(String section) throws ClassNotFoundException, SQLException {
		Connection conn = DBUtil.getConnection();

		String sqlInsert = "INSERT INTO `sport_service_sections`(`caption`) VALUES (?)";
		PreparedStatement psInsert = conn.prepareStatement(sqlInsert);
		psInsert.setString(1, section);
		psInsert.execute();

		conn.close();
	}

}