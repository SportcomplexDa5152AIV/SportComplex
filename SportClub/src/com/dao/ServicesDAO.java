package com.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.database.DBUtil;
import com.gs.Service;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class ServicesDAO {

	private static String getString(String sql) {
		String text = null;

		try {
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sql);
			while (result.next()) {
				text = result.getString(1);
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return text;
	}

	public static ObservableList<String> getSectionFromDB() {
		ObservableList<String> data = FXCollections.observableArrayList();

		data.add("Все");
		try {
			String sqlGetSection = "SELECT `caption` FROM `sport_service_sections` WHERE `status` = 1";
			Connection conn = DBUtil.getConnection();
			ResultSet result = conn.createStatement().executeQuery(sqlGetSection);
			while (result.next()) {
				data.add(result.getString(1));
			}
			result.close();
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return data;
	}

	public static ObservableList<Service> getTableValuesFromDB(String sql) throws ClassNotFoundException, SQLException {
		ObservableList<Service> data = FXCollections.observableArrayList();

		Connection conn = DBUtil.getConnection();
		ResultSet result = conn.createStatement().executeQuery(sql);
		while (result.next()) {
			String doctors = null;
			if (result.getInt(7) == 0) {
				doctors = "Не требуется";
			} else {
				doctors = "Требуется";
			}

			data.add(new Service(result.getInt(1),
					getString("SELECT `caption` FROM `sport_service_sections` WHERE `id` = '" + result.getInt(3) + "'"),
					result.getString(2), result.getDouble(4), result.getDouble(5), result.getDouble(6), doctors));
		}
		result.close();
		conn.close();

		return data;
	}

	public static void deleteService(int id) throws SQLException, ClassNotFoundException {
		String sqlDelete = "UPDATE `sport_services` SET `status` = 0 WHERE `id` = '" + id + "'";
		Connection conn = DBUtil.getConnection();
		Statement stmt = conn.createStatement();
		stmt.executeUpdate(sqlDelete);
		conn.close();
	}
	
}