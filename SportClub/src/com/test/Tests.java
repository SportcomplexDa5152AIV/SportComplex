package com.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.add.Validation;

public class Tests {

	@Test
	public void testMail() {
		Validation validation = new Validation();
		boolean checkMail = validation.isValidEmailAddress("&@rtyuio");
		assertTrue("NOT MAIL", !checkMail);
	}
	
	@Test
	public void testMD5() {
		Validation validation = new Validation();
		String checkMD5 = validation.md5Apache("9402964502683");
		assertEquals("25a2e73a2f8481338b0bcf75e23fe007", checkMD5);
	}

	@Test
	public void testNumeric() {
		Validation validation = new Validation();
		boolean checkNumeric = validation.isNumeric("-5");
		assertTrue("NOT POS NUMERIC", !checkNumeric);
	}

	@Test
	public void testTel() {
		Validation validation = new Validation();
		boolean checkTel = validation.isTelephone("32232/111");
		assertTrue("NOT TEL", !checkTel);
	}

	@Test
	public void testAlphabeticWithoutSpace() {
		Validation validation = new Validation();
		boolean checkTel = validation.isAlphabeticWithoutSpace("Ffghj6");
		assertTrue("NOT ALPHABETIc", !checkTel);
	}

}