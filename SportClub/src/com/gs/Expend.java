package com.gs;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Expend {

	private final IntegerProperty id;
	private final StringProperty date;
	private final StringProperty name;
	private final DoubleProperty price;

	public Expend(Integer id, String name, String date, Double price) {
		this.name = new SimpleStringProperty(name);
		this.id = new SimpleIntegerProperty(id);
		this.price = new SimpleDoubleProperty(price);
		this.date = new SimpleStringProperty(date);
	}

	public Integer getId() {
		return id.get();
	}

	public IntegerProperty idProperty() {
		return id;
	}

	public void setId(Integer id) {
		this.id.set(id);
	}

	public String getName() {
		return name.get();
	}

	public StringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public String getDate() {
		return date.get();
	}

	public StringProperty dateProperty() {
		return date;
	}

	public void setSection(String date) {
		this.date.set(date);
	}

	public Double getPrice() {
		return price.get();
	}

	public DoubleProperty priceProperty() {
		return price;
	}

	public void setPrice(Double price) {
		this.price.set(price);
	}

}