package com.gs;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Admin {

	private final StringProperty login;
	private final StringProperty password;
	private final StringProperty loginMail;
	private final StringProperty passwordMail;
	private final StringProperty loginSms;
	private final StringProperty passwordSms;
	private final StringProperty alpha;
	
	public Admin(String login, String password, String loginMail, String passwordMail, String loginSms, String passwordSms, String alpha){
		this.login = new SimpleStringProperty(login);
		this.password = new SimpleStringProperty(password);
		this.loginMail = new SimpleStringProperty(loginMail);
		this.loginSms = new SimpleStringProperty(loginSms);
		this.passwordSms = new SimpleStringProperty(passwordSms);
		this.passwordMail = new SimpleStringProperty(passwordMail);
		this.alpha = new SimpleStringProperty(alpha);
	}
	
	public String getPassword() {
		return password.get();
	}

	public StringProperty passwordProperty() {
		return password;
	}

	public void setPassword(String password) {
		this.password.set(password);
	}

	public String getLogin() {
		return login.get();
	}

	public StringProperty loginProperty() {
		return login;
	}

	public void setLogin(String login) {
		this.login.set(login);
	}
	
	public String getLoginMail() {
		return loginMail.get();
	}

	public StringProperty loginMailProperty() {
		return loginMail;
	}

	public void setLoginMail(String loginMail) {
		this.loginMail.set(loginMail);
	}
	
	public String getPasswordMail() {
		return passwordMail.get();
	}

	public StringProperty passwordMailProperty() {
		return passwordMail;
	}

	public void setPasswordMail(String passwordMail) {
		this.passwordMail.set(passwordMail);
	}
	
	public String getLoginSms() {
		return loginSms.get();
	}

	public StringProperty loginSmsProperty() {
		return loginSms;
	}

	public void setLoginSms(String loginSms) {
		this.loginSms.set(loginSms);
	}
	
	public String getPasswordSms() {
		return passwordSms.get();
	}

	public StringProperty passwordSmsProperty() {
		return passwordSms;
	}

	public void setPasswordSms(String passwordSms) {
		this.passwordSms.set(passwordSms);
	}
	
	public String getAlpha() {
		return alpha.get();
	}

	public StringProperty alphaProperty() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha.set(alpha);
	}
	
}