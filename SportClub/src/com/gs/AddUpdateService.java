package com.gs;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class AddUpdateService {

	private final StringProperty section;
	private final StringProperty name;
	private final DoubleProperty once;
	private final DoubleProperty month;
	private final DoubleProperty year;
	private final IntegerProperty doctors;

	public AddUpdateService(String section, String name, Double once, Double month, Double year, Integer doctors) {
		this.doctors = new SimpleIntegerProperty(doctors);
		this.name = new SimpleStringProperty(name);
		this.once = new SimpleDoubleProperty(once);
		this.month = new SimpleDoubleProperty(month);
		this.year = new SimpleDoubleProperty(year);
		this.section = new SimpleStringProperty(section);
	}

	public String getName() {
		return name.get();
	}

	public StringProperty nameProperty() {
		return name;
	}

	public void setName(String name) {
		this.name.set(name);
	}

	public Integer getDoctors() {
		return doctors.get();
	}

	public IntegerProperty doctorsProperty() {
		return doctors;
	}

	public void setDoctors(Integer doctots) {
		this.doctors.set(doctots);
	}

	public String getSection() {
		return section.get();
	}

	public StringProperty sectionProperty() {
		return section;
	}

	public void setSection(String section) {
		this.section.set(section);
	}

	public Double getOnce() {
		return once.get();
	}

	public DoubleProperty onceProperty() {
		return once;
	}

	public void setOnce(Double once) {
		this.once.set(once);
	}
	
	public Double getMonth() {
		return month.get();
	}

	public DoubleProperty monthProperty() {
		return month;
	}

	public void setMonth(Double month) {
		this.month.set(month);
	}
	
	public Double getYear() {
		return year.get();
	}

	public DoubleProperty yearProperty() {
		return year;
	}

	public void setYear(Double year) {
		this.year.set(year);
	}
	
}