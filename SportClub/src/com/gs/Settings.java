package com.gs;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Settings {

	private final StringProperty login;
	private final StringProperty loginMail;
	private final StringProperty loginSms;
	private final StringProperty alpha;
	
	public Settings(String login, String loginMail, String loginSms, String alpha){
		this.login = new SimpleStringProperty(login);
		this.loginMail = new SimpleStringProperty(loginMail);
		this.loginSms = new SimpleStringProperty(loginSms);
		this.alpha = new SimpleStringProperty(alpha);
	}

	public String getLogin() {
		return login.get();
	}

	public StringProperty loginProperty() {
		return login;
	}

	public void setLogin(String login) {
		this.login.set(login);
	}
	
	public String getLoginMail() {
		return loginMail.get();
	}

	public StringProperty loginMailProperty() {
		return loginMail;
	}

	public void setLoginMail(String loginMail) {
		this.loginMail.set(loginMail);
	}
	
	public String getLoginSms() {
		return loginSms.get();
	}

	public StringProperty loginSmsProperty() {
		return loginSms;
	}

	public void setLoginSms(String loginSms) {
		this.loginSms.set(loginSms);
	}
	
	public String getAlpha() {
		return alpha.get();
	}

	public StringProperty alphaProperty() {
		return alpha;
	}

	public void setAlpha(String alpha) {
		this.alpha.set(alpha);
	}
	
}