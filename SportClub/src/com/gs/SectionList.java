package com.gs;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class SectionList {

	private final IntegerProperty id;
	private final StringProperty section;

	public SectionList(Integer id, String section) {
		this.id = new SimpleIntegerProperty(id);
		this.section = new SimpleStringProperty(section);
	}

	public String getSection() {
		return section.get();
	}

	public StringProperty sectionProperty() {
		return section;
	}

	public void setSection(String section) {
		this.section.set(section);
	}

	public Integer getId() {
		return id.get();
	}

	public IntegerProperty idProperty() {
		return id;
	}

	public void setId(Integer id) {
		this.id.set(id);
	}
	
}