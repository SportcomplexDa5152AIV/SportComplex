package com.controller;

import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.dao.EditServiceDAO;
import com.gs.AddUpdateService;
import com.gs.Service;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class EditServiceController {

	@FXML
	private ComboBox<String> sectionBox;
	@FXML
	private TextField nameField;
	@FXML
	private TextField onceField;
	@FXML
	private TextField monthField;
	@FXML
	private TextField yearField;
	@FXML
	private CheckBox doctorsCheck;

	private Service service;

	public void setService(Service service) {
		this.service = service;
		sectionBox.setValue(service.getSection());
		nameField.setText(service.getName());
		onceField.setText(service.getOnce().toString());
		monthField.setText(service.getMonth().toString());
		yearField.setText(service.getYear().toString());
		if (service.getDoctors().equals("Требуется")) {
			doctorsCheck.setSelected(true);
		} else {
			doctorsCheck.setSelected(false);
		}
	}

	@FXML
	private void saveAction(ActionEvent event) throws NumberFormatException, ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (sectionBox.getValue() != null && !nameField.getText().isEmpty() && !onceField.getText().isEmpty()
				&& !monthField.getText().isEmpty() && !yearField.getText().isEmpty()) {
			if (nameField.getText().length() > 70) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Наименование услуги' слишком большой длины!");
			} else if (!validation.isNumeric(onceField.getText()) || onceField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Разовая цена' неправильного формата!");
			} else if (!validation.isNumeric(monthField.getText()) || monthField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Месячная цена' неправильного формата!");
			} else if (!validation.isNumeric(yearField.getText()) || yearField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Годовая цена' неправильного формата!");
			} else if (EditServiceDAO.checkIfServiceContainsInDBBeforeUpdate(service.getId(), nameField.getText())) {
				validation.alertWarning("Ошибка", null, "Услуга с таким наименованием уже существует!");
			} else {
				int doctors = 0;
				if (doctorsCheck.isSelected()) {
					doctors = 1;
				}

				EditServiceDAO.updateService(service.getId(),
						new AddUpdateService(sectionBox.getValue(), nameField.getText(),
								validation.round(Double.parseDouble(onceField.getText()), 2),
								validation.round(Double.parseDouble(monthField.getText()), 2),
								validation.round(Double.parseDouble(yearField.getText()), 2), doctors));

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Данные успешно изменены!");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
				Stage stage2 = (Stage) nameField.getScene().getWindow();
				stage2.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) nameField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}