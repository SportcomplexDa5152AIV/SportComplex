package com.controller;

import com.gs.Clients;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;

public class ClientsOneController {

	@FXML
	private Rectangle rectangle;
	@FXML
	private ComboBox<String> sexBox;
	@FXML
	private TextField phoneField;
	@FXML
	private ImageView searchImage;
	@FXML
	private Button searchButton;
	@FXML
	private TableView<Clients> mainTable;
	@FXML
	private TableColumn<Clients, Integer> numberColumn;
	@FXML
	private TableColumn<Clients, String> nameColumn;
	@FXML
	private TableColumn<Clients, String> phoneColumn;
	@FXML
	private TableColumn<Clients, String> birthdayColumn;
	@FXML
	private TableColumn<Clients, String> sexColumn;

	public void initialize() {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		searchImage.setImage(new Image("file:src/resources/search.png"));
		
		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});
		
		sexBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		nameColumn.setStyle("-fx-alignment: CENTER;");
		phoneColumn.setStyle("-fx-alignment: CENTER;");
		birthdayColumn.setStyle("-fx-alignment: CENTER;");
		sexColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		nameColumn.setResizable(false);
		phoneColumn.setResizable(false);
		birthdayColumn.setResizable(false);
		sexColumn.setResizable(false);

		numberColumn.setPrefWidth(visualBounds.getWidth() * 0.04);
		nameColumn.setPrefWidth(visualBounds.getWidth() * 0.36);
		phoneColumn.setPrefWidth(visualBounds.getWidth() * 0.17);
		birthdayColumn.setPrefWidth(visualBounds.getWidth() * 0.11);
		sexColumn.setPrefWidth(visualBounds.getWidth() * 0.12);
	}

	public void sort() {

	}

	@FXML
	private void keyAction(KeyEvent event) throws Exception {
		if (event.getCode() == KeyCode.ENTER) {
			sort();
		}
	}

	@FXML
	private void addClientAction(ActionEvent event) {

	}

}