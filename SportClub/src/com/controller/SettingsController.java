package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.SettingsDAO;
import com.gs.Settings;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class SettingsController implements Initializable {

	@FXML
	private TextField loginField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private PasswordField passwordAgainField;
	@FXML
	private TextField loginSmsField;
	@FXML
	private PasswordField passwordSmsField;
	@FXML
	private TextField alphaField;
	@FXML
	private TextField loginMailField;
	@FXML
	private PasswordField passwordMailField;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Settings settings = SettingsDAO.getSettingsValues();
		loginField.setText(settings.getLogin());
		loginMailField.setText(settings.getLoginMail());
		loginSmsField.setText(settings.getLoginSms());
		alphaField.setText(settings.getAlpha());
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!passwordField.getText().isEmpty() && passwordAgainField.getText().isEmpty()) {
			validation.alertWarning("Ошибка", "Неверный формат полей", "Введите пароль, пожалуйста!");
		} else if (passwordField.getText().isEmpty() && !passwordAgainField.getText().isEmpty()) {
			validation.alertWarning("Ошибка", "Неверный формат полей", "Повторите пароль, пожалуйста!");
		} else if (!loginField.getText().isEmpty() && loginField.getText().length() > 50) {
			validation.alertWarning("Ошибка", "Неверный формат полей", "Логин слишком большой длины!");
		} else if (!passwordField.getText().isEmpty()
				&& (passwordField.getText().length() > 30 || passwordField.getText().length() < 4)) {
			validation.alertWarning("Ошибка", "Неверный формат полей",
					"Пароль должен быть длиной от 4 до 30 символов!");
		} else if (!passwordField.getText().isEmpty()
				&& !passwordField.getText().equals(passwordAgainField.getText())) {
			validation.alertWarning("Ошибка", null, "Пароли не совпадают!");
		} else if (!alphaField.getText().isEmpty() && alphaField.getText().length() > 11) {
			validation.alertWarning("Ошибка", "Неверный формат полей",
					"Поле 'Alpha-имя' должно быть длиной до 11 символов!");
		} else if (!loginSmsField.getText().isEmpty()
				&& (loginSmsField.getText().contains("+") || !validation.isTelephone(loginSmsField.getText()))) {
			validation.alertWarning("Ошибка", "Неверный формат полей",
					"Поле логин(для sms рассылки) неправильного формата!");
		} else if (!loginSmsField.getText().isEmpty() && loginSmsField.getText().length() != 12) {
			validation.alertWarning("Ошибка", "Неверный формат полей",
					"Поле логин(для sms рассылки) должно быть длиной в 12 символов!");
		} else if (!loginMailField.getText().isEmpty() && loginMailField.getText().length() > 70) {
			validation.alertWarning("Ошибка", "Неверный формат полей",
					"Поле логин(для e-mail рассылки) слишком большой длины!");
		} else if (!loginMailField.getText().isEmpty() && !validation.isValidEmailAddress(loginMailField.getText())) {
			validation.alertWarning("Ошибка", "Неверный формат полей",
					"Поле логин(для e-mail рассылки) неправильного формата!");
		} else {
			if (!loginField.getText().isEmpty()) {
				SettingsDAO.updateAction("UPDATE `admin_login` SET `login` = '" + loginField.getText() + "'");
			}

			if (!passwordField.getText().isEmpty()) {
				SettingsDAO.updateAction("UPDATE `admin_login` SET `password` = '"
						+ validation.md5Apache(passwordField.getText()) + "'");
			}

			if (!loginMailField.getText().isEmpty()) {
				SettingsDAO.updateAction("UPDATE `admin_login` SET `mail` = '" + loginMailField.getText() + "'");
			}

			if (!passwordMailField.getText().isEmpty()) {
				SettingsDAO.updateAction(
						"UPDATE `admin_login` SET `password_mail` = '" + passwordMailField.getText() + "'");
			}

			if (!loginSmsField.getText().isEmpty()) {
				SettingsDAO.updateAction("UPDATE `admin_login` SET `login_sms` = '" + loginSmsField.getText() + "'");
			}

			if (!passwordSmsField.getText().isEmpty()) {
				SettingsDAO
						.updateAction("UPDATE `admin_login` SET `password_sms` = '" + passwordSmsField.getText() + "'");
			}

			if (!alphaField.getText().isEmpty()) {
				SettingsDAO.updateAction("UPDATE `admin_login` SET `alpha_sms` = '" + alphaField.getText() + "'");
			}

			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Уведомление");
			alert.setHeaderText(null);
			alert.setContentText("Данные успешно изменены!");
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
			alert.showAndWait();
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) loginField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Настройки'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}