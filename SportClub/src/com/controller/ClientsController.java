package com.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Clock;
import com.main.AllScenes;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ClientsController implements Initializable{

	@FXML
	private Label timeLabel;
	@FXML
	private Label unreadCountLabel;
	@FXML
	private ImageView closeImage;
	@FXML
	private ImageView abonementsImage;
	@FXML
	private ImageView servicesImage;
	@FXML
	private ImageView clientsImage;
	@FXML
	private ImageView expenditureImage;
	@FXML
	private ImageView statisticsImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView notificationImage;
	@FXML
	private ImageView settingsImage;
	@FXML
	private TabPane tabPane;
	@FXML
	private Tab clients;
	@FXML
	private Tab sms;
	@FXML
	private ClientsOneController clientsOneController;
	@FXML
	private ClientsTwoController clientsTwoController;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		abonementsImage.setImage(new Image("file:src/resources/abonImage.png"));
		servicesImage.setImage(new Image("file:src/resources/servicesImage.png"));
		clientsImage.setImage(new Image("file:src/resources/clientsImage.png"));
		expenditureImage.setImage(new Image("file:src/resources/expendImage.png"));
		notificationImage.setImage(new Image("file:src/resources/notificationImage.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));
		statisticsImage.setImage(new Image("file:src/resources/statisticsImage.png"));
		settingsImage.setImage(new Image("file:src/resources/settingsImage.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);
		
		tabPane.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Tab>() {
			@Override
			public void changed(ObservableValue<? extends Tab> observable, Tab oldValue, Tab newValue) {
				if (newValue == clients) {
					clientsOneController.initialize();
					clientsOneController.sort();
				} else if (newValue == sms) {
					clientsTwoController.initialize();
					clientsTwoController.sort();
				}
			}
		});
	}
	
	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}
	
	@FXML
	private void abonementsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.abonementsScene);
	}
	
	@FXML
	private void servicesAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.servicesScene);
	}

	@FXML
	private void expenditureAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.expenditureScene);
	}

	@FXML
	private void statisticsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.statisticsScene);
	}

	@FXML
	private void settingsAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/settings.fxml", 805, "Настройки",
				"Вы действительно хотите закрыть окно 'Настройки'?");
	}

	@FXML
	private void notificationAction(ActionEvent event) {

	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}