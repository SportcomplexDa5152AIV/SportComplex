package com.controller;

import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Validation;
import com.dao.AddServiceDAO;
import com.gs.AddUpdateService;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class AddServiceController implements Initializable {

	@FXML
	private ComboBox<String> sectionBox;
	@FXML
	private TextField nameField;
	@FXML
	private TextField onceField;
	@FXML
	private TextField monthField;
	@FXML
	private TextField yearField;
	@FXML
	private CheckBox doctorsCheck;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		sectionBox.setButtonCell(new ListCell<String>() {
			@Override
			protected void updateItem(String item, boolean empty) {
				super.updateItem(item, empty);
				if (empty || item == null) {
					setStyle("-fx-text-fill: #8a8a8a;");
				} else {
					setStyle("-fx-text-fill: -fx-text-inner-color");
					setText(item);
				}
			}
		});

		sectionBox.setItems(AddServiceDAO.getListOfSectionsFromDB());
	}

	@FXML
	private void saveAction(ActionEvent event) throws NumberFormatException, ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (sectionBox.getValue() != null && !nameField.getText().isEmpty() && !onceField.getText().isEmpty()
				&& !monthField.getText().isEmpty() && !yearField.getText().isEmpty()) {
			if (nameField.getText().length() > 70) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Наименование услуги' слишком большой длины!");
			} else if (!validation.isNumeric(onceField.getText()) || onceField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Разовая цена' неправильного формата!");
			} else if (!validation.isNumeric(monthField.getText()) || monthField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Месячная цена' неправильного формата!");
			} else if (!validation.isNumeric(yearField.getText()) || yearField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Годовая цена' неправильного формата!");
			} else if (AddServiceDAO.checkIfServiceContainsInDBBeforeInsert(nameField.getText())) {
				validation.alertWarning("Ошибка", null, "Услуга с таким наименованием уже существует!");
			} else {
				int doctors = 0;
				if (doctorsCheck.isSelected()) {
					doctors = 1;
				}

				AddServiceDAO.insertIntoPaperDB(new AddUpdateService(sectionBox.getValue(), nameField.getText(),
						validation.round(Double.parseDouble(onceField.getText()), 2),
						validation.round(Double.parseDouble(monthField.getText()), 2),
						validation.round(Double.parseDouble(yearField.getText()), 2), doctors));

				Stage stage = (Stage) nameField.getScene().getWindow();
				stage.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) nameField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Создание'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}