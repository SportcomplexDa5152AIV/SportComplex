package com.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Clock;
import com.main.AllScenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class StatisticsController implements Initializable {

	@FXML
	private Rectangle rectangle;
	@FXML
	private Label timeLabel;
	@FXML
	private Label unreadCountLabel;
	@FXML
	private ImageView closeImage;
	@FXML
	private ImageView abonementsImage;
	@FXML
	private ImageView servicesImage;
	@FXML
	private ImageView clientsImage;
	@FXML
	private ImageView expenditureImage;
	@FXML
	private ImageView statisticsImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView notificationImage;
	@FXML
	private ImageView settingsImage;
	@FXML
	private PieChart pieChart;
	@FXML
	private RadioButton allTime;
	@FXML
	private DatePicker fromField;
	@FXML
	private DatePicker endField;
	@FXML
	private AnchorPane pane;

	private static final int WIDTH = 500;
	private static final int HEIGHT = 400;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fitMax();
		
		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		abonementsImage.setImage(new Image("file:src/resources/abonImage.png"));
		servicesImage.setImage(new Image("file:src/resources/servicesImage.png"));
		clientsImage.setImage(new Image("file:src/resources/clientsImage.png"));
		expenditureImage.setImage(new Image("file:src/resources/expendImage.png"));
		notificationImage.setImage(new Image("file:src/resources/notificationImage.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));
		statisticsImage.setImage(new Image("file:src/resources/statisticsImage.png"));
		settingsImage.setImage(new Image("file:src/resources/settingsImage.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);

		percent();
		
		allTime.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				fromField.setValue(null);
				endField.setValue(null);
				allTime.setSelected(true);
				sort();
			}
		});

		fromField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});

		endField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});
	}

	private void fitMax(){		
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);
	}
	
	private void sort() {

	}
	
	private void percent() {
		Label caption = new Label("");
		caption.setTextFill(Color.BLACK);
		caption.setStyle("-fx-font: 24 arial;");

		for (final PieChart.Data data : pieChart.getData()) {
			data.getNode().addEventHandler(MouseEvent.MOUSE_ENTERED, e -> {
				double total = 0;
				for (PieChart.Data d : pieChart.getData()) {
					total += d.getPieValue();
				}
				caption.setTranslateX(e.getSceneX() - WIDTH / 2);
				caption.setTranslateY(e.getSceneY() - HEIGHT / 2);
				String text = String.format("%.1f%%", 100 * data.getPieValue() / total);
				caption.setText(text);
				caption.setVisible(true);
			});
			data.getNode().addEventHandler(MouseEvent.MOUSE_EXITED, e -> {
				caption.setVisible(false);
			});
		}
		pane.getChildren().addAll(caption);
	}

	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}
	
	@FXML
	private void abonementsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.abonementsScene);
	}
	
	@FXML
	private void servicesAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.servicesScene);
	}

	@FXML
	private void expenditureAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.expenditureScene);
	}

	@FXML
	private void clientsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.clientsScene);
	}

	@FXML
	private void settingsAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/settings.fxml", 805, "Настройки",
				"Вы действительно хотите закрыть окно 'Настройки'?");
	}

	@FXML
	private void notificationAction(ActionEvent event) {

	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}
	
}