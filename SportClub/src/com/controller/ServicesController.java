package com.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Clock;
import com.dao.ServicesDAO;
import com.gs.Service;
import com.main.AllScenes;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ServicesController implements Initializable {

	@FXML
	private Rectangle rectangle;
	@FXML
	private Label timeLabel;
	@FXML
	private ComboBox<String> sectionBox;
	@FXML
	private CheckBox doctorsCheck;
	@FXML
	private TableView<Service> mainTable;
	@FXML
	private TableColumn<Service, Integer> numberColumn;
	@FXML
	private TableColumn<Service, String> nameColumn;
	@FXML
	private TableColumn<Service, String> sectionColumn;
	@FXML
	private TableColumn<Service, Double> onceColumn;
	@FXML
	private TableColumn<Service, Double> monthColumn;
	@FXML
	private TableColumn<Service, Double> yearColumn;
	@FXML
	private TableColumn<Service, String> doctorsColumn;
	@FXML
	private Label unreadCountLabel;
	@FXML
	private ImageView closeImage;
	@FXML
	private ImageView abonementsImage;
	@FXML
	private ImageView servicesImage;
	@FXML
	private ImageView clientsImage;
	@FXML
	private ImageView expenditureImage;
	@FXML
	private ImageView statisticsImage;
	@FXML
	private ImageView deleteImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView notificationImage;
	@FXML
	private ImageView settingsImage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		abonementsImage.setImage(new Image("file:src/resources/abonImage.png"));
		servicesImage.setImage(new Image("file:src/resources/servicesImage.png"));
		clientsImage.setImage(new Image("file:src/resources/clientsImage.png"));
		expenditureImage.setImage(new Image("file:src/resources/expendImage.png"));
		deleteImage.setImage(new Image("file:src/resources/769.png"));
		notificationImage.setImage(new Image("file:src/resources/notificationImage.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));
		statisticsImage.setImage(new Image("file:src/resources/statisticsImage.png"));
		settingsImage.setImage(new Image("file:src/resources/settingsImage.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);

		fillSectionBox();

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});

		ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Редактировать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FXMLLoader fxmlLoader = new FXMLLoader();
				Parent fxmlEdit = null;
				EditServiceController editServiceController = new EditServiceController();
				try {
					fxmlLoader.setLocation(getClass().getResource("/com/models/editService.fxml"));
					fxmlEdit = fxmlLoader.load();
					editServiceController = fxmlLoader.getController();
				} catch (IOException e) {
				}

				Service selectedService = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Редактирование");
				al.setHeaderText("Редактируем услугу?");
				al.setContentText(
						"Вы уверены, что хотите редактировать услугу '" + selectedService.getName() + "' ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					editServiceController.setService(selectedService);
					Stage editStage = new Stage();
					editStage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					editStage.setTitle("Редактирование");
					editStage.setWidth(505);
					editStage.setResizable(false);
					editStage.setScene(new Scene(fxmlEdit));
					editStage.initModality(Modality.WINDOW_MODAL);
					editStage.initOwner(AllScenes.primaryStage);
					editStage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
						@Override
						public void handle(javafx.stage.WindowEvent event) {
							Alert al = new Alert(Alert.AlertType.CONFIRMATION);
							al.setTitle("Закрытие");
							al.setHeaderText(null);
							al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
							Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
							Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
							okButton.setText("Да");
							Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
							cancelButton.setText("Нет");
							stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
							Optional<ButtonType> result = al.showAndWait();
							if (result.get() == ButtonType.OK) {
								editStage.close();
							} else {
								al.close();
								event.consume();
							}
						}
					});
					editStage.showAndWait();
					if (editStage.getScene().getWindow().isShowing() == false) {
						sort();
					}
					mainTable.getSelectionModel().clearSelection();
				} else {
					al.close();
				}
			}
		});
		MenuItem item2 = new MenuItem("Удалить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Service selectedService = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Удаление");
				al.setHeaderText(null);
				al.setContentText(
						"Вы уверены, что хотите удалить удалить услугу '" + selectedService.getName() + "' ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					try {
						ServicesDAO.deleteService(selectedService.getId());
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}

					sort();
				} else {
					al.close();
				}
			}
		});
		contextMenu.getItems().addAll(item1, item2);

		mainTable.setRowFactory(new Callback<TableView<Service>, TableRow<Service>>() {
			@Override
			public TableRow<Service> call(TableView<Service> tableView) {
				TableRow<Service> row = new TableRow<>();
				row.contextMenuProperty()
						.bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));
				return row;
			}
		});

		sectionBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		doctorsCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		nameColumn.setStyle("-fx-alignment: CENTER;");
		sectionColumn.setStyle("-fx-alignment: CENTER;");
		onceColumn.setStyle("-fx-alignment: CENTER;");
		monthColumn.setStyle("-fx-alignment: CENTER;");
		yearColumn.setStyle("-fx-alignment: CENTER;");
		doctorsColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		nameColumn.setResizable(false);
		sectionColumn.setResizable(false);
		onceColumn.setResizable(false);
		monthColumn.setResizable(false);
		yearColumn.setResizable(false);
		doctorsColumn.setResizable(false);
	}

	private void fillSectionBox() {
		sectionBox.setItems(ServicesDAO.getSectionFromDB());
	}

	private void loadTable(String sql) {
		nameColumn.setCellValueFactory(new PropertyValueFactory<Service, String>("name"));
		sectionColumn.setCellValueFactory(new PropertyValueFactory<Service, String>("section"));
		onceColumn.setCellValueFactory(new PropertyValueFactory<Service, Double>("once"));
		monthColumn.setCellValueFactory(new PropertyValueFactory<Service, Double>("month"));
		yearColumn.setCellValueFactory(new PropertyValueFactory<Service, Double>("year"));
		doctorsColumn.setCellValueFactory(new PropertyValueFactory<Service, String>("doctors"));

		mainTable.setItems(null);
		try {
			mainTable.setItems(ServicesDAO.getTableValuesFromDB(sql));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sort() {
		if (sectionBox.getValue() != null && !sectionBox.getValue().equals("Все")) {
			if (doctorsCheck.isSelected()) {
				loadTable(
						"SELECT * FROM `sport_services` USE INDEX(`section_need_status_idx`) WHERE `service_section_id` IN (SELECT `id` FROM `sport_service_sections` WHERE `caption` = '"
								+ sectionBox.getValue() + "') AND `need_clinic` = 1 AND `status` = 1");
			} else {
				loadTable(
						"SELECT * FROM `sport_services` USE INDEX(`section_id_status_idx`) WHERE `service_section_id` IN (SELECT `id` FROM `sport_service_sections` WHERE `caption` = '"
								+ sectionBox.getValue() + "') AND `status` = 1");
			}
		} else {
			if (doctorsCheck.isSelected()) {
				loadTable(
						"SELECT * FROM `sport_services` USE INDEX(`need_status_idx`) WHERE `need_clinic` = 1 AND `status` = 1");
			} else {
				loadTable("SELECT * FROM `sport_services` USE INDEX(`status_idx`) WHERE `status` = 1");
			}
		}
	}

	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}

	@FXML
	private void addSectionAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/addSection.fxml", 505, "Создание",
				"Вы действительно хотите закрыть окно 'Создание'?");

		fillSectionBox();
		sort();
	}

	@FXML
	private void sectionAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/sectionList.fxml", 505, "Просмотр",
				"Вы действительно хотите закрыть окно просмотра?");

		fillSectionBox();
		sort();
	}

	@FXML
	private void addServiceAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/addService.fxml", 505, "Создание",
				"Вы действительно хотите закрыть окно 'Создание'?");

		sort();
	}

	@FXML
	private void abonementsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.abonementsScene);
	}

	@FXML
	private void clientsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.clientsScene);
	}

	@FXML
	private void expenditureAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.expenditureScene);
	}

	@FXML
	private void statisticsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.statisticsScene);
	}

	@FXML
	private void settingsAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/settings.fxml", 805, "Настройки",
				"Вы действительно хотите закрыть окно 'Настройки'?");
	}

	@FXML
	private void notificationAction(ActionEvent event) {

	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}