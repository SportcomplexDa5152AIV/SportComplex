package com.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellUtil;

import com.add.Clock;
import com.dao.ExpenditureDAO;
import com.gs.Expend;
import com.main.AllScenes;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.binding.Bindings;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ExpenditureController implements Initializable {

	@FXML
	private Rectangle rectangle;
	@FXML
	private Label timeLabel;
	@FXML
	private DatePicker fromField;
	@FXML
	private DatePicker endField;
	@FXML
	private RadioButton allTime;
	@FXML
	private TableView<Expend> mainTable;
	@FXML
	private TableColumn<Expend, Integer> numberColumn;
	@FXML
	private TableColumn<Expend, String> dateColumn;
	@FXML
	private TableColumn<Expend, String> nameColumn;
	@FXML
	private TableColumn<Expend, Double> priceColumn;
	@FXML
	private Label unreadCountLabel;
	@FXML
	private ImageView excelImage;
	@FXML
	private ImageView closeImage;
	@FXML
	private ImageView abonementsImage;
	@FXML
	private ImageView servicesImage;
	@FXML
	private ImageView clientsImage;
	@FXML
	private ImageView expenditureImage;
	@FXML
	private ImageView statisticsImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView notificationImage;
	@FXML
	private ImageView settingsImage;

	private ObservableList<Expend> dataTable;
	private DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy");

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		abonementsImage.setImage(new Image("file:src/resources/abonImage.png"));
		servicesImage.setImage(new Image("file:src/resources/servicesImage.png"));
		clientsImage.setImage(new Image("file:src/resources/clientsImage.png"));
		expenditureImage.setImage(new Image("file:src/resources/expendImage.png"));
		excelImage.setImage(new Image("file:src/resources/excel.png"));
		notificationImage.setImage(new Image("file:src/resources/notificationImage.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));
		statisticsImage.setImage(new Image("file:src/resources/statisticsImage.png"));
		settingsImage.setImage(new Image("file:src/resources/settingsImage.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});

		ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Редактировать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FXMLLoader fxmlLoader = new FXMLLoader();
				Parent fxmlEdit = null;
				EditExpendController editExpendController = new EditExpendController();
				try {
					fxmlLoader.setLocation(getClass().getResource("/com/models/editExpend.fxml"));
					fxmlEdit = fxmlLoader.load();
					editExpendController = fxmlLoader.getController();
				} catch (IOException e) {
				}

				Expend selectedExpend = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Редактирование");
				al.setHeaderText("Редактируем расход?");
				al.setContentText("Вы уверены, что хотите редактировать расход '" + selectedExpend.getName() + " за "
						+ selectedExpend.getDate() + " ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					editExpendController.setExpend(selectedExpend);
					Stage editStage = new Stage();
					editStage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					editStage.setTitle("Редактирование");
					editStage.setWidth(505);
					editStage.setResizable(false);
					editStage.setScene(new Scene(fxmlEdit));
					editStage.initModality(Modality.WINDOW_MODAL);
					editStage.initOwner(AllScenes.primaryStage);
					editStage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
						@Override
						public void handle(javafx.stage.WindowEvent event) {
							Alert al = new Alert(Alert.AlertType.CONFIRMATION);
							al.setTitle("Закрытие");
							al.setHeaderText(null);
							al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
							Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
							Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
							okButton.setText("Да");
							Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
							cancelButton.setText("Нет");
							stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
							Optional<ButtonType> result = al.showAndWait();
							if (result.get() == ButtonType.OK) {
								editStage.close();
							} else {
								al.close();
								event.consume();
							}
						}
					});
					editStage.showAndWait();
					if (editStage.getScene().getWindow().isShowing() == false) {
						sort();
					}
					mainTable.getSelectionModel().clearSelection();
				} else {
					al.close();
				}
			}
		});
		MenuItem item2 = new MenuItem("Удалить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Expend selectedExpend = mainTable.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Удаление");
				al.setHeaderText(null);
				al.setContentText("Вы уверены, что хотите удалить расход '" + selectedExpend.getName() + "' за "
						+ selectedExpend.getDate() + " ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					try {
						ExpenditureDAO.deleteFromExpendDB(selectedExpend.getId());
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}

					sort();
				} else {
					al.close();
				}
			}
		});
		contextMenu.getItems().addAll(item1, item2);

		mainTable.setRowFactory(new Callback<TableView<Expend>, TableRow<Expend>>() {
			@Override
			public TableRow<Expend> call(TableView<Expend> tableView) {
				TableRow<Expend> row = new TableRow<>();
				row.contextMenuProperty()
						.bind(Bindings.when(row.emptyProperty()).then((ContextMenu) null).otherwise(contextMenu));
				return row;
			}
		});

		allTime.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				fromField.setValue(null);
				endField.setValue(null);
				allTime.setSelected(true);
				sort();
			}
		});

		fromField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});

		endField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		nameColumn.setStyle("-fx-alignment: CENTER;");
		dateColumn.setStyle("-fx-alignment: CENTER;");
		priceColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		dateColumn.setResizable(false);
		nameColumn.setResizable(false);
		priceColumn.setResizable(false);

		numberColumn.setPrefWidth(visualBounds.getWidth() * 0.04);
		dateColumn.setPrefWidth(visualBounds.getWidth() * 0.11);
		nameColumn.setPrefWidth(visualBounds.getWidth() * 0.5);
		priceColumn.setPrefWidth(visualBounds.getWidth() * 0.15);
	}

	private void loadTable(String sql) {
		dataTable = FXCollections.observableArrayList();
		try {
			dataTable.setAll(ExpenditureDAO.getTableValuesFromDB(sql));
		} catch (ClassNotFoundException | SQLException e1) {
			e1.printStackTrace();
		}

		nameColumn.setCellValueFactory(new PropertyValueFactory<Expend, String>("name"));
		dateColumn.setCellValueFactory(new PropertyValueFactory<Expend, String>("date"));
		priceColumn.setCellValueFactory(new PropertyValueFactory<Expend, Double>("price"));

		mainTable.setItems(null);
		try {
			mainTable.setItems(dataTable);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void sort() {
		if (allTime.isSelected()) {
			loadTable("SELECT * FROM `sport_balance_outputs` USE INDEX() ORDER BY `date` DESC");
		} else {
			if (fromField.getValue() != null && endField.getValue() != null) {
				loadTable("SELECT * FROM `sport_balance_outputs` USE INDEX(`date_idx`) WHERE `date` BETWEEN '"
						+ fromField.getValue() + "' AND '" + endField.getValue() + "' ORDER BY `date` DESC");
			} else if (fromField.getValue() != null && endField.getValue() == null) {
				loadTable("SELECT * FROM `sport_balance_outputs` USE INDEX(`date_idx`) WHERE `date` >= '"
						+ fromField.getValue() + "' ORDER BY `date` DESC");
			} else if (fromField.getValue() == null && endField.getValue() != null) {
				loadTable("SELECT * FROM `sport_balance_outputs` USE INDEX(`date_idx`) WHERE `date` <= '"
						+ endField.getValue() + "' ORDER BY `date` DESC");
			} else if (fromField.getValue() == null && endField.getValue() == null) {
				loadTable("SELECT * FROM `sport_balance_outputs` USE INDEX() ORDER BY `date` DESC");
			}
		}
	}

	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}

	@FXML
	private void addExpendAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/addExpend.fxml", 505, "Создание",
				"Вы действительно хотите закрыть окно 'Создание'?");

		sort();
	}

	@SuppressWarnings("deprecation")
	@FXML
	private void excelAction(ActionEvent event) throws IOException {
		Node source = (Node) event.getSource();
		Stage parentStage = (Stage) source.getScene().getWindow();

		@SuppressWarnings("resource")
		HSSFWorkbook wb = new HSSFWorkbook();
		HSSFSheet sheet = wb.createSheet("Отчет по расходам");

		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(CellUtil.BORDER_TOP, CellStyle.BORDER_MEDIUM);
		properties.put(CellUtil.BORDER_BOTTOM, CellStyle.BORDER_MEDIUM);
		properties.put(CellUtil.BORDER_LEFT, CellStyle.BORDER_MEDIUM);
		properties.put(CellUtil.BORDER_RIGHT, CellStyle.BORDER_MEDIUM);

		Map<String, Object> properties2 = new HashMap<String, Object>();
		properties2.put(CellUtil.BORDER_TOP, CellStyle.BORDER_THIN);
		properties2.put(CellUtil.BORDER_BOTTOM, CellStyle.BORDER_THIN);
		properties2.put(CellUtil.BORDER_LEFT, CellStyle.BORDER_THIN);
		properties2.put(CellUtil.BORDER_RIGHT, CellStyle.BORDER_THIN);

		HSSFCellStyle style = wb.createCellStyle();
		style.setWrapText(true);

		HSSFCellStyle style2 = wb.createCellStyle();
		HSSFFont font2 = wb.createFont();
		font2.setFontName("Arial");
		font2.setFontHeight((short) 250);
		font2.setBold(true);
		font2.setColor(HSSFColor.BLACK.index);
		style2.setFont(font2);

		HSSFCellStyle styleHeader = wb.createCellStyle();
		styleHeader.setWrapText(true);
		styleHeader.setFillForegroundColor(IndexedColors.AQUA.getIndex());
		styleHeader.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		HSSFFont fontHeader = wb.createFont();
		fontHeader.setFontName("Arial");
		fontHeader.setFontHeight((short) 200);
		fontHeader.setBold(true);
		fontHeader.setColor(HSSFColor.BLACK.index);
		styleHeader.setFont(fontHeader);

		HSSFRow name = sheet.createRow(0);
		String headerValue = null;
		if (this.allTime.isSelected()) {
			headerValue = "за весь период";
		} else {
			if (this.fromField.getValue() != null && this.endField.getValue() == null) {
				headerValue = "за период с " + this.fromField.getValue().format(dateFormat);
			} else if (this.fromField.getValue() == null && this.endField.getValue() != null) {
				headerValue = "за период по " + this.endField.getValue().format(dateFormat);
			} else if (this.fromField.getValue() != null && this.endField.getValue() != null) {
				headerValue = "за период с " + this.fromField.getValue().format(dateFormat) + " по "
						+ this.endField.getValue().format(dateFormat);
			} else if (this.fromField.getValue() == null && this.endField.getValue() == null) {
				headerValue = "за весь период";
			}
		}
		name.createCell(0).setCellValue("Отчет по расходам " + headerValue);
		name.setHeight((short) 800);
		sheet.addMergedRegion(new CellRangeAddress(0, 0, 0, 3));
		name.getCell(0).setCellStyle(style2);
		CellUtil.setAlignment(name.getCell(0), HorizontalAlignment.CENTER);
		CellUtil.setVerticalAlignment((Cell) name.getCell(0), VerticalAlignment.CENTER);

		HSSFRow header = sheet.createRow(1);
		header.setHeight((short) 500);
		header.createCell(0).setCellValue("№");
		sheet.setColumnWidth(0, 2500);
		header.createCell(1).setCellValue("Дата");
		sheet.setColumnWidth(1, 3000);
		header.createCell(2).setCellValue("Название");
		sheet.setColumnWidth(2, 8000);
		header.createCell(3).setCellValue("Сумма");
		sheet.setColumnWidth(3, 4500);
		for (int i = 0; i < 4; i++) {
			header.getCell(i).setCellStyle(styleHeader);
			CellUtil.setAlignment(header.getCell(i), HorizontalAlignment.CENTER);
			CellUtil.setVerticalAlignment((Cell) header.getCell(i), VerticalAlignment.CENTER);
			CellUtil.setCellStyleProperties(header.getCell(i), properties);
		}

		for (int i = 0; i < dataTable.size(); i++) {
			HSSFRow row = sheet.createRow(i + 2);
			row.createCell(0).setCellValue(i + 1);
			row.createCell(1).setCellValue(dataTable.get(i).getDate());
			row.createCell(2).setCellValue(dataTable.get(i).getName());
			row.createCell(3).setCellValue(dataTable.get(i).getPrice());
			for (int j = 0; j < 4; j++) {
				row.getCell(j).setCellStyle(style);
				CellUtil.setCellStyleProperties(row.getCell(j), properties2);
				CellUtil.setVerticalAlignment((Cell) row.getCell(j), VerticalAlignment.CENTER);
				CellUtil.setAlignment(row.getCell(j), HorizontalAlignment.CENTER);
			}
		}

		FileOutputStream fileOut = new FileOutputStream("expend.xls");
		wb.write(fileOut);
		fileOut.close();

		File fileData = new File("expend.xls");
		FileChooser chooser = new FileChooser();
		chooser.setTitle("Отчет по расходам");
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Excel files (*.xls)", "*.xls");
		chooser.getExtensionFilters().add(extFilter);
		File selectedFolder = chooser.showSaveDialog(parentStage);
		if (selectedFolder != null) {
			try {
				Files.copy(fileData.toPath(), selectedFolder.toPath(), StandardCopyOption.COPY_ATTRIBUTES);

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Отчет по расходам успешно создан!");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		fileData.delete();
	}

	@FXML
	private void abonementsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.abonementsScene);
	}

	@FXML
	private void servicesAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.servicesScene);
	}

	@FXML
	private void clientsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.clientsScene);
	}

	@FXML
	private void statisticsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.statisticsScene);
	}

	@FXML
	private void settingsAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/settings.fxml", 805, "Настройки",
				"Вы действительно хотите закрыть окно 'Настройки'?");
	}

	@FXML
	private void notificationAction(ActionEvent event) {

	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}