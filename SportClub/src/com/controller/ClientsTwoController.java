package com.controller;

import com.gs.Sender;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Rectangle2D;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.shape.Rectangle;
import javafx.stage.Screen;

public class ClientsTwoController {

	@FXML
	private Rectangle rectangle;
	@FXML
	private ComboBox<String> sectionBox;
	@FXML
	private DatePicker fromField;
	@FXML
	private DatePicker endField;
	@FXML
	private RadioButton allTime;
	@FXML
	private TableView<Sender> mainTable;
	@FXML
	private TableColumn<Sender, Integer> numberColumn;
	@FXML
	private TableColumn<Sender, String> dateColumn;
	@FXML
	private TableColumn<Sender, String> timeColumn;
	@FXML
	private TableColumn<Sender, String> nameColumn;
	@FXML
	private TableColumn<Sender, String> sectionColumn;
	@FXML
	private TableColumn<Sender, Integer> countColumn;

	public void initialize() {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});
		
		sectionBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});
		
		allTime.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				fromField.setValue(null);
				endField.setValue(null);
				allTime.setSelected(true);
				sort();
			}
		});

		fromField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});

		endField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		nameColumn.setStyle("-fx-alignment: CENTER;");
		dateColumn.setStyle("-fx-alignment: CENTER;");
		timeColumn.setStyle("-fx-alignment: CENTER;");
		sectionColumn.setStyle("-fx-alignment: CENTER;");
		countColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		nameColumn.setResizable(false);
		dateColumn.setResizable(false);
		timeColumn.setResizable(false);
		sectionColumn.setResizable(false);
		countColumn.setResizable(false);

		numberColumn.setPrefWidth(visualBounds.getWidth() * 0.04);
		nameColumn.setPrefWidth(visualBounds.getWidth() * 0.36);
		dateColumn.setPrefWidth(visualBounds.getWidth() * 0.11);
		timeColumn.setPrefWidth(visualBounds.getWidth() * 0.07);
		sectionColumn.setPrefWidth(visualBounds.getWidth() * 0.12);
		countColumn.setPrefWidth(visualBounds.getWidth() * 0.1);
	}

	public void sort() {

	}

	@FXML
	private void addSenderAction(ActionEvent event) {

	}

}