package com.controller;

import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.dao.AddSectionDAO;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class AddSectionController {

	@FXML
	private TextField sectionField;

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (sectionField.getText().isEmpty()) {
			validation.alertWarning("Ошибка", null, "Заполните поле нового раздела, пожалуйста");
		} else if (sectionField.getText().length() > 40) {
			validation.alertWarning("Ошибка", "Неверный формат полей!",
					"Значение поля 'Раздел' слишком большой длины!");
		} else if (AddSectionDAO.checkIfSectionContainsInDBBeforeInsert(sectionField.getText())) {
			validation.alertWarning("Ошибка", null, "Раздел с таким наименованием уже существует!");
		} else {
			AddSectionDAO.insertIntoDB(sectionField.getText());

			Stage stage = (Stage) sectionField.getScene().getWindow();
			stage.close();
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) sectionField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Создание'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}