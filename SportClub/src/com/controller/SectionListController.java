package com.controller;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Optional;
import java.util.ResourceBundle;

import com.dao.SectionListDAO;
import com.gs.SectionList;
import com.main.AllScenes;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.input.ContextMenuEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class SectionListController implements Initializable {

	@FXML
	private ListView<SectionList> listView;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		fillListView();

		listView.setCellFactory(param -> new ListCell<SectionList>() {
			@Override
			protected void updateItem(SectionList item, boolean empty) {
				super.updateItem(item, empty);

				if (empty || item == null || item.getSection() == null) {
					setText(null);
				} else {
					setText(item.getSection());
				}
			}
		});

		ContextMenu contextMenu = new ContextMenu();
		MenuItem item1 = new MenuItem("Редактировать");
		item1.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				FXMLLoader fxmlLoader = new FXMLLoader();
				Parent fxmlEdit = null;
				EditSectionController editSectionController = new EditSectionController();
				try {
					fxmlLoader.setLocation(getClass().getResource("/com/models/editSection.fxml"));
					fxmlEdit = fxmlLoader.load();
					editSectionController = fxmlLoader.getController();
				} catch (IOException e) {
				}

				SectionList selectedSection = listView.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Редактирование");
				al.setHeaderText("Редактируем раздел?");
				al.setContentText(
						"Вы уверены, что хотите редактировать раздел '" + selectedSection.getSection() + "' ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					editSectionController.setSectiolList(selectedSection);
					Stage editStage = new Stage();
					editStage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
					editStage.setTitle("Редактирование");
					editStage.setWidth(505);
					editStage.setResizable(false);
					editStage.setScene(new Scene(fxmlEdit));
					editStage.initModality(Modality.WINDOW_MODAL);
					editStage.initOwner(AllScenes.primaryStage);
					editStage.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
						@Override
						public void handle(javafx.stage.WindowEvent event) {
							Alert al = new Alert(Alert.AlertType.CONFIRMATION);
							al.setTitle("Закрытие");
							al.setHeaderText(null);
							al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
							Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
							Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
							okButton.setText("Да");
							Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
							cancelButton.setText("Нет");
							stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
							Optional<ButtonType> result = al.showAndWait();
							if (result.get() == ButtonType.OK) {
								editStage.close();
							} else {
								al.close();
								event.consume();
							}
						}
					});
					editStage.showAndWait();
					if (editStage.getScene().getWindow().isShowing() == false) {
						fillListView();
					}
					listView.getSelectionModel().clearSelection();
				} else {
					al.close();
				}
			}
		});
		MenuItem item2 = new MenuItem("Удалить");
		item2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				SectionList selectedSection = listView.getSelectionModel().getSelectedItem();
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Удаление");
				al.setHeaderText(null);
				al.setContentText("Вы уверены, что хотите удалить раздел '" + selectedSection + "' ?");
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					al.close();
					try {
						SectionListDAO.deleteSection(selectedSection.getId());
					} catch (ClassNotFoundException | SQLException e) {
						e.printStackTrace();
					}

					fillListView();
				} else {
					al.close();
				}
			}
		});
		contextMenu.getItems().addAll(item1, item2);

		listView.setContextMenu(contextMenu);
		listView.setOnContextMenuRequested(new EventHandler<ContextMenuEvent>() {
			@Override
			public void handle(ContextMenuEvent event) {
				contextMenu.show(listView, event.getScreenX(), event.getScreenY());
				event.consume();
			}
		});
	}

	private void fillListView() {
		listView.setItems(SectionListDAO.fillSectionListFromDB());
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) listView.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно просмотра?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}