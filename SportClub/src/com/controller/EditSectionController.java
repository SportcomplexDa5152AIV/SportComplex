package com.controller;

import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.dao.EditSectionDAO;
import com.gs.SectionList;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class EditSectionController {

	@FXML
	private TextField sectionField;

	private SectionList sectionList;

	public void setSectiolList(SectionList sectionList) {
		this.sectionList = sectionList;
		sectionField.setText(sectionList.getSection());
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (sectionField.getText().isEmpty()) {
			validation.alertWarning("Ошибка", null, "Заполните поле раздела, пожалуйста");
		} else if (sectionField.getText().length() > 40) {
			validation.alertWarning("Ошибка", "Неверный формат полей!",
					"Значение поля 'Раздел' слишком большой длины!");
		} else if (EditSectionDAO.checkIfSectionContainsInDBBeforeUpdate(sectionList.getId(), sectionField.getText())) {
			validation.alertWarning("Ошибка", null, "Раздел с таким наименованием уже существует!");
		} else {
			EditSectionDAO.updateIntoDB(sectionList.getId(), sectionField.getText());

			Alert alert = new Alert(Alert.AlertType.INFORMATION);
			alert.setTitle("Уведомление");
			alert.setHeaderText(null);
			alert.setContentText("Данные успешно изменены!");
			Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
			stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
			alert.showAndWait();
			Stage stage2 = (Stage) sectionField.getScene().getWindow();
			stage2.close();
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) sectionField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Редактирование'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}