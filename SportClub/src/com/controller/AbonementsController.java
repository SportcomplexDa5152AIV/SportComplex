package com.controller;

import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

import com.add.Clock;
import com.gs.Abonements;
import com.main.AllScenes;
import com.sun.javafx.scene.control.skin.TableHeaderRow;

import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.util.Callback;

public class AbonementsController implements Initializable {

	@FXML
	private Rectangle rectangle;
	@FXML
	private Label timeLabel;
	@FXML
	private ComboBox<String> sectionBox;
	@FXML
	private ComboBox<String> statusBox;
	@FXML
	private DatePicker fromField;
	@FXML
	private DatePicker endField;
	@FXML
	private RadioButton allTime;
	@FXML
	private TextField numberField;
	@FXML
	private Button searchButton;
	@FXML
	private TableView<Abonements> mainTable;
	@FXML
	private TableColumn<Abonements, Integer> numberColumn;
	@FXML
	private TableColumn<Abonements, String> abonNumberColumn;
	@FXML
	private TableColumn<Abonements, String> servicesColumn;
	@FXML
	private TableColumn<Abonements, String> sectionColumn;
	@FXML
	private TableColumn<Abonements, Double> priceColumn;
	@FXML
	private TableColumn<Abonements, String> statusColumn;
	@FXML
	private TableColumn<Abonements, String> beginDateColumn;
	@FXML
	private TableColumn<Abonements, String> endDateColumn;
	@FXML
	private TableColumn<Abonements, String> clientColumn;
	@FXML
	private TableColumn<Abonements, Double> leftPriceColumn;
	@FXML
	private TableColumn<Abonements, Double> allPriceColumn;
	@FXML
	private Label unreadCountLabel;
	@FXML
	private ImageView searchImage;
	@FXML
	private ImageView excelImage;
	@FXML
	private ImageView closeImage;
	@FXML
	private ImageView abonementsImage;
	@FXML
	private ImageView servicesImage;
	@FXML
	private ImageView clientsImage;
	@FXML
	private ImageView expenditureImage;
	@FXML
	private ImageView statisticsImage;
	@FXML
	private ImageView triangleImage;
	@FXML
	private ImageView notificationImage;
	@FXML
	private ImageView settingsImage;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		mainTable.setPlaceholder(new Label(null));

		fitMax();

		triangleImage.setImage(new Image("file:src/resources/triangle.png"));
		abonementsImage.setImage(new Image("file:src/resources/abonImage.png"));
		servicesImage.setImage(new Image("file:src/resources/servicesImage.png"));
		clientsImage.setImage(new Image("file:src/resources/clientsImage.png"));
		expenditureImage.setImage(new Image("file:src/resources/expendImage.png"));
		excelImage.setImage(new Image("file:src/resources/excel.png"));
		searchImage.setImage(new Image("file:src/resources/search.png"));
		notificationImage.setImage(new Image("file:src/resources/notificationImage.png"));
		closeImage.setImage(new Image("file:src/resources/exit.png"));
		statisticsImage.setImage(new Image("file:src/resources/statisticsImage.png"));
		settingsImage.setImage(new Image("file:src/resources/settingsImage.png"));

		Clock clock = new Clock();
		clock.setFont(new Font("System", 36));
		timeLabel.setGraphic(clock);

		servicesColumn.setCellFactory(new Callback<TableColumn<Abonements, String>, TableCell<Abonements, String>>() {
			@Override
			public TableCell<Abonements, String> call(TableColumn<Abonements, String> param) {
				TableCell<Abonements, String> cell = new TableCell<>();
				Text text = new Text();
				cell.setGraphic(text);
				cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
				text.wrappingWidthProperty().bind(cell.widthProperty());
				text.textProperty().bind(cell.itemProperty());
				return cell;
			}
		});

		numberColumn.setCellValueFactory(
				column -> new ReadOnlyObjectWrapper<Integer>(mainTable.getItems().indexOf(column.getValue()) + 1));
		numberColumn.setSortable(false);

		sort();

		mainTable.widthProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> source, Number oldWidth, Number newWidth) {
				TableHeaderRow header = (TableHeaderRow) mainTable.lookup("TableHeaderRow");
				header.reorderingProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
							Boolean newValue) {
						header.setReordering(false);
					}
				});
			}
		});

		sectionBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		statusBox.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		searchButton.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				sort();
			}
		});

		allTime.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				fromField.setValue(null);
				endField.setValue(null);
				allTime.setSelected(true);
				sort();
			}
		});

		fromField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});

		endField.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (allTime.isSelected()) {
					allTime.setSelected(false);
				}
				sort();
			}
		});
	}

	private void fitMax() {
		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		rectangle.setWidth(visualBounds.getWidth() - 120);

		numberColumn.setStyle("-fx-alignment: CENTER;");
		abonNumberColumn.setStyle("-fx-alignment: CENTER;");
		sectionColumn.setStyle("-fx-alignment: CENTER;");
		servicesColumn.setStyle("-fx-alignment: CENTER;");
		priceColumn.setStyle("-fx-alignment: CENTER;");
		statusColumn.setStyle("-fx-alignment: CENTER;");
		beginDateColumn.setStyle("-fx-alignment: CENTER;");
		endDateColumn.setStyle("-fx-alignment: CENTER;");
		clientColumn.setStyle("-fx-alignment: CENTER;");
		leftPriceColumn.setStyle("-fx-alignment: CENTER;");
		allPriceColumn.setStyle("-fx-alignment: CENTER;");

		numberColumn.setResizable(false);
		abonNumberColumn.setResizable(false);
		sectionColumn.setResizable(false);
		servicesColumn.setResizable(false);
		priceColumn.setResizable(false);
		statusColumn.setResizable(false);
		beginDateColumn.setResizable(false);
		endDateColumn.setResizable(false);
		clientColumn.setResizable(false);
		leftPriceColumn.setResizable(false);
		allPriceColumn.setResizable(false);
	}

	private void loadTable(String sql) {

	}

	private void sort() {

	}

	@FXML
	private void keyAction(KeyEvent event) throws Exception {
		if (event.getCode() == KeyCode.ENTER) {
			sort();
		}
	}

	private void openWindowFunction(String url, double width, String title, String alert) throws IOException {
		Parent root = FXMLLoader.load(getClass().getResource(url));
		Stage stage2 = new Stage();
		stage2.setWidth(width);
		stage2.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage2.setTitle(title);
		stage2.setResizable(false);
		Scene scene = new Scene(root);
		stage2.setScene(scene);
		stage2.initOwner(AllScenes.primaryStage);
		stage2.initModality(Modality.WINDOW_MODAL);
		stage2.setOnCloseRequest(new EventHandler<javafx.stage.WindowEvent>() {
			@Override
			public void handle(javafx.stage.WindowEvent event) {
				Alert al = new Alert(Alert.AlertType.CONFIRMATION);
				al.setTitle("Закрытие");
				al.setHeaderText(null);
				al.setContentText(alert);
				Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
				Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
				okButton.setText("Да");
				Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
				cancelButton.setText("Нет");
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				Optional<ButtonType> result = al.showAndWait();
				if (result.get() == ButtonType.OK) {
					stage2.close();
				} else {
					al.close();
					event.consume();
				}
			}
		});
		stage2.showAndWait();
	}

	@FXML
	private void addAbonAction(ActionEvent event) {

	}

	@FXML
	private void saleAbonAction(ActionEvent event) {

	}

	@FXML
	private void excelAction(ActionEvent event) {

	}

	@FXML
	private void servicesAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.servicesScene);
	}

	@FXML
	private void clientsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.clientsScene);
	}

	@FXML
	private void expenditureAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.expenditureScene);
	}

	@FXML
	private void statisticsAction(ActionEvent event) {
		AllScenes.primaryStage.setScene(AllScenes.statisticsScene);
	}

	@FXML
	private void settingsAction(ActionEvent event) throws IOException {
		openWindowFunction("/com/models/settings.fxml", 805, "Настройки",
				"Вы действительно хотите закрыть окно 'Настройки'?");
	}

	@FXML
	private void notificationAction(ActionEvent event) {

	}

	@FXML
	private void logoutAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}