package com.controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

import com.add.Validation;
import com.dao.EditExpendDAO;
import com.gs.AddUpdateExpend;
import com.gs.Expend;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class EditExpendController {

	@FXML
	private TextField nameField;
	@FXML
	private DatePicker dateField;
	@FXML
	private TextField priceField;

	private Expend expend;

	public void setExpend(Expend expend) {
		this.expend = expend;
		nameField.setText(expend.getName());
		dateField.setValue(LocalDate.parse(expend.getDate(), DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		priceField.setText(expend.getPrice().toString());
	}

	@FXML
	private void saveAction(ActionEvent event) throws NumberFormatException, ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!nameField.getText().isEmpty() && dateField.getValue() != null && !priceField.getText().isEmpty()) {
			if (nameField.getText().length() > 200) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Название' слишком большой длины!");
			} else if (!validation.isNumeric(priceField.getText()) || priceField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Сумма' неправильного формата!");
			} else if (dateField.getValue().isAfter(LocalDate.now())) {
				validation.alertWarning("Ошибка", "Неверный формат полей!",
						"Значение поля 'Дата расхода' не может превышать сегодняшний день!");
			} else {
				EditExpendDAO.updateExpendIntoDB(expend.getId(),
						new AddUpdateExpend(nameField.getText(), dateField.getValue().toString(),
								validation.round(Double.parseDouble(priceField.getText()), 2)));

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Данные успешно изменены!");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
				Stage stage2 = (Stage) nameField.getScene().getWindow();
				stage2.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все должные поля, пожалуйста");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Stage parentStage = (Stage) nameField.getScene().getWindow();
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Редакирование'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		stage.initOwner(parentStage);
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			parentStage.close();
		} else {
			al.close();
		}
	}

}