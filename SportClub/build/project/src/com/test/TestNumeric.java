package com.test;

import static org.junit.Assert.*;

import org.junit.Test;

import com.add.Validation;

public class TestNumeric {

	@Test
	public void test() {
		Validation validation = new Validation();
		boolean checkNumeric = validation.isNumeric("-5");
		assertTrue("NOT POS NUMERIC", !checkNumeric);
	}

}