package com.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.add.Validation;

public class TestTel {

	@Test
	public void test() {
		Validation validation = new Validation();
		boolean checkTel = validation.isTelephone("32232/111");
		assertTrue("NOT TEL", !checkTel);
	}
	
}