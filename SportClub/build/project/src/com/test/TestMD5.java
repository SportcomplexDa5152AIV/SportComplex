package com.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.add.Validation;

public class TestMD5 {

	@Test
	public void test() {
		Validation validation = new Validation();
		String checkMD5 = validation.md5Apache("9402964502683");
		assertEquals("25a2e73a2f8481338b0bcf75e23fe007", checkMD5);
	}
	
}