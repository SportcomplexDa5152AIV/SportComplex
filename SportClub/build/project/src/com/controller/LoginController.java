package com.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.database.DBUtil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;

public class LoginController {

	@FXML
	private TextField loginField;
	@FXML
	private PasswordField passwordField;

	private Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();

	@FXML
	private void changePassword(ActionEvent event) throws IOException {
		Stage stage = new Stage();
		Parent root = FXMLLoader.load(getClass().getResource("/com/models/changePassword.fxml"));
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		stage.setTitle("Смена пароля");
		stage.setX(visualBounds.getMinX());
		stage.setY(visualBounds.getMinY());
		stage.setWidth(visualBounds.getWidth());
		stage.setHeight(visualBounds.getHeight());
		stage.setResizable(false);
		Scene scene = new Scene(root);
		stage.setScene(scene);
		Node source = (Node) event.getSource();
		Stage parentStage = (Stage) source.getScene().getWindow();
		stage.initOwner(parentStage);
		stage.initModality(Modality.WINDOW_MODAL);
		stage.showAndWait();
	}

	private boolean checkLogin(String login, String password) throws SQLException, ClassNotFoundException {
		boolean checker = false;

		Connection conn = DBUtil.getConnection();
		String sqlGetKey = "SELECT * FROM `admin_login` WHERE `login` = '" + login + "' AND `password` = '" + password
				+ "'";
		ResultSet result = conn.createStatement().executeQuery(sqlGetKey);
		while (result.next()) {
			checker = true;
		}
		result.close();
		conn.close();

		return checker;
	}

	private void login() throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!loginField.getText().isEmpty() && !passwordField.getText().isEmpty()) {
			if (!checkLogin(loginField.getText(), validation.md5Apache(passwordField.getText()))) {
				validation.alertWarning("Ошибка", null,
						"Введен неверный логин или пароль. Повторите попытку, пожалуйста");
			} else {

			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все поля, пожалуйста");
		}
	}

	@FXML
	private void keyAction(KeyEvent event) throws Exception {
		if (event.getCode() == KeyCode.ENTER) {
			login();
		}
	}

	@FXML
	private void enterAction(ActionEvent event) throws Exception {
		login();
	}

	@FXML
	private void closeAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы уверены, что хотите выйти?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}