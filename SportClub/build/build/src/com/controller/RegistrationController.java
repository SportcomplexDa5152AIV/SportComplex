package com.controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.database.DBUtil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class RegistrationController {

	@FXML
	private TextField loginField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private PasswordField passwordAgainField;
	@FXML
	private PasswordField pinField;
	@FXML
	private TextField loginSmsField;
	@FXML
	private PasswordField passwordSmsField;
	@FXML
	private TextField alphaField;
	@FXML
	private TextField loginMailField;
	@FXML
	private PasswordField passwordMailField;

	@FXML
	private void saveAction(ActionEvent event) throws SQLException, ClassNotFoundException {
		Validation validation = new Validation();
		if (!loginField.getText().isEmpty() && !passwordField.getText().isEmpty()
				&& !passwordAgainField.getText().isEmpty() && !pinField.getText().isEmpty()) {
			if (loginField.getText().length() > 50) {
				validation.alertWarning("Ошибка", "Неверный формат полей", "Логин слишком большой длины!");
			} else if (passwordField.getText().length() > 30 || passwordField.getText().length() < 4) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Пароль должен быть длиной от 4 до 30 символов!");
			} else if (!passwordField.getText().equals(passwordAgainField.getText())) {
				validation.alertWarning("Ошибка", null, "Пароли не совпадают!");
			} else if (!validation.isTelephone(pinField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей", "Поле 'PIN-ключ' неправильного формата!");
			} else if (pinField.getText().length() > 10) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Значение поля 'PIN-ключ' слишком большой длины!");
			} else if (!alphaField.getText().isEmpty() && alphaField.getText().length() > 11) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Поле 'Alpha-имя' должно быть длиной до 11 символов!");
			} else if (!loginSmsField.getText().isEmpty()
					&& (loginSmsField.getText().contains("+") || !validation.isTelephone(loginSmsField.getText()))) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Поле логин(для sms рассылки) неправильного формата!");
			} else if (!loginSmsField.getText().isEmpty() && loginSmsField.getText().length() != 12) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Поле логин(для sms рассылки) должно быть длиной в 12 символов!");
			} else if (!loginMailField.getText().isEmpty() && loginMailField.getText().length() > 70) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Поле логин(для e-mail рассылки) слишком большой длины!");
			} else if (!loginMailField.getText().isEmpty()
					&& !validation.isValidEmailAddress(loginMailField.getText())) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Поле логин(для e-mail рассылки) неправильного формата!");
			} else {
				Connection conn = DBUtil.getConnection();

				String sqlInsertLogin = "INSERT INTO `admin_login`(`login`,`password`,`pin_key`,`mail`,`password_mail`,`login_sms`,`password_sms`,`alpha_sms`) VALUES (?,?,?,?,?,?,?,?)";
				PreparedStatement psInsertLogin = conn.prepareStatement(sqlInsertLogin);
				psInsertLogin.setString(1, loginField.getText());
				psInsertLogin.setString(2, validation.md5Apache(passwordField.getText()));
				psInsertLogin.setString(3, validation.md5Apache(pinField.getText()));
				psInsertLogin.setString(4, loginMailField.getText());
				psInsertLogin.setString(5, passwordMailField.getText());
				psInsertLogin.setString(6, loginSmsField.getText());
				psInsertLogin.setString(7, passwordSmsField.getText());
				psInsertLogin.setString(8, alphaField.getText());
				psInsertLogin.execute();

				conn.close();

				Alert alert = new Alert(Alert.AlertType.INFORMATION);
				alert.setTitle("Уведомление");
				alert.setHeaderText(null);
				alert.setContentText("Регистрация прошла успешно.\nПерезагрузите программу, пожалуйста.");
				Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				alert.showAndWait();
				Node source = (Node) event.getSource();
				Stage parentStage = (Stage) source.getScene().getWindow();
				parentStage.close();
				System.exit(0);
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните все поля, пожалуйста!");
		}
	}

	@FXML
	private void cancelAction(ActionEvent event) {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Регистрация'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}