package com.controller;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import com.add.Validation;
import com.database.DBUtil;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class KeyController {

	@FXML
	private TextField keyField;

	private boolean checkKey(String key) throws SQLException, ClassNotFoundException {
		boolean checker = false;

		Connection conn = DBUtil.getConnection();
		String sqlGetKey = "SELECT * FROM `admin_key` WHERE `key` = '" + key + "'";
		ResultSet result = conn.createStatement().executeQuery(sqlGetKey);
		while (result.next()) {
			checker = true;
		}
		result.close();
		conn.close();

		return checker;
	}

	@FXML
	private void nextAction() throws ClassNotFoundException, SQLException, IOException {
		Validation validation = new Validation();
		if (!keyField.getText().isEmpty()) {
			if (keyField.getText().length() > 50) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Значение поля ввода ключа слишком большой длины!");
			} else if (!checkKey(validation.md5Apache(keyField.getText()))) {
				validation.alertWarning("Ошибка", null, "Введен неверный ключ!");
			} else {
				Parent root = FXMLLoader.load(getClass().getResource("/com/models/registration.fxml"));
				Stage stage = new Stage();
				stage.setWidth(800);
				stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
				stage.setTitle("Регистрация");
				stage.setResizable(false);
				Scene scene = new Scene(root);
				stage.setScene(scene);
				Stage parentStage = (Stage) keyField.getScene().getWindow();
				stage.initOwner(parentStage);
				stage.initModality(Modality.WINDOW_MODAL);
				stage.showAndWait();
				parentStage.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Введите ключ, пожалуйста!");
		}
	}

	@FXML
	private void cancelAction() {
		Alert al = new Alert(Alert.AlertType.CONFIRMATION);
		al.setTitle("Выход");
		al.setHeaderText(null);
		al.setContentText("Вы действительно хотите закрыть окно 'Регистрация'?");
		Stage stage = (Stage) al.getDialogPane().getScene().getWindow();
		Button okButton = (Button) al.getDialogPane().lookupButton(ButtonType.OK);
		okButton.setText("Да");
		Button cancelButton = (Button) al.getDialogPane().lookupButton(ButtonType.CANCEL);
		cancelButton.setText("Нет");
		stage.getIcons().add(new Image("file:src/resources/mainIcon.png"));
		Optional<ButtonType> result = al.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			al.close();
		}
	}

}