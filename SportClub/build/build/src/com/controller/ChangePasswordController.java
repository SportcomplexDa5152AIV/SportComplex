package com.controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.add.Validation;
import com.database.DBUtil;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ChangePasswordController {

	@FXML
	private TextField keyField;
	@FXML
	private PasswordField passwordField;
	@FXML
	private PasswordField passwordFieldAgain;

	private boolean checkKey(String key) throws SQLException, ClassNotFoundException {
		boolean checker = false;

		Connection conn = DBUtil.getConnection();
		String sqlGetKey = "SELECT * FROM `admin_key` WHERE `key` = '" + key + "'";
		ResultSet result = conn.createStatement().executeQuery(sqlGetKey);
		while (result.next()) {
			checker = true;
		}
		result.close();
		conn.close();

		return checker;
	}

	@FXML
	private void saveAction(ActionEvent event) throws ClassNotFoundException, SQLException {
		Validation validation = new Validation();
		if (!keyField.getText().isEmpty() && !passwordField.getText().isEmpty()
				&& !passwordFieldAgain.getText().isEmpty()) {
			if (!checkKey(validation.md5Apache(keyField.getText()))) {
				validation.alertWarning("Ошибка", null, "Введен неверный ключ!");
			} else if (!passwordField.getText().equals(passwordFieldAgain.getText())) {
				validation.alertWarning("Ошибка", null, "Пароли не совпадают!");
			} else if (passwordField.getText().length() < 4 || passwordField.getText().length() > 30) {
				validation.alertWarning("Ошибка", "Неверный формат полей",
						"Значение поля 'Новый пароль' должно быть длиной от 4 до 30 символов!");
			} else {
				String sqlUpdate = "UPDATE `admin_login` SET `password` = '"
						+ validation.md5Apache(passwordField.getText()) + "'";
				Connection conn = DBUtil.getConnection();
				Statement stmt = conn.createStatement();
				stmt.executeUpdate(sqlUpdate);
				conn.close();

				Node source = (Node) event.getSource();
				Stage parentStage = (Stage) source.getScene().getWindow();
				parentStage.close();
			}
		} else {
			validation.alertWarning("Ошибка", null, "Заполните вся поля, пожалуйста!");
		}
	}

	@FXML
	private void closeAction(ActionEvent event) {
		Node source = (Node) event.getSource();
		Stage parentStage = (Stage) source.getScene().getWindow();
		parentStage.close();
	}

}