package com.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.database.DBUtil;

@RunWith(Suite.class)
@SuiteClasses({TestNumeric.class, TestMail.class, TestTel.class, DBUtil.class, TestMD5.class})
public class AllTests {

}